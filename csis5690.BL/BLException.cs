﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csis5690.BL
{
    public class BLException : Exception
    {
        public BLException() : base("Something bad happened in the business layer.")
        {

        }

        public BLException(string message) : base(message)
        {

        }
    }
}
