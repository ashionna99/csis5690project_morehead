﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using csis5690.DAL;
using csis5690.Domain;

namespace csis5690.BL
{
    public class SoftwareAppBO : BaseBO
    {
        protected SoftwareAppDAO dao;

        //constructor
        public SoftwareAppBO() : this(DEFAULT_CONNECTION_KEY)
        {

        }

        public SoftwareAppBO(string connectionKey) : base(connectionKey)
        {
            dao = new SoftwareAppDAO(this.connectionKey = connectionKey);
        }


        public override object SelectOneObject(object filter)
        {
            return dao.SelectOneObject(filter);
        }

        public override IList<object> SelectManyObjects(object obj)
        {
            return dao.SelectManyObjects(obj);
        }

        public override object InsertOneObject(object obj)
        {
            return dao.InsertOneObject(obj);
        }

        public override object UpdateOneObject(object obj)
        {
            return dao.UpdateOneObject(obj);
        }

        public override object DeleteOneObject(object obj)
        {
            return dao.DeleteOneObject(obj);
        }


        public override void InsertManyObjects()
        {
            
        }

        public override void UpdateManyObjects()
        {
            
        }

        public override void DeleteManyObjects()
        {
            
        }
    }
}
