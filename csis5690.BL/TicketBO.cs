﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using csis5690.DAL;
using csis5690.Domain;

namespace csis5690.BL
{
    public class TicketBO : BaseBO
    {
        private TicketDAO dao;
        private BugDAO bugDAO;
        private StatusDAO statusDAO;
        private PriorityDAO priorityDAO;
        private SoftwareAppDAO appDAO;

        //constructor
        public TicketBO() : this(DEFAULT_CONNECTION_KEY)
        {

        }

        public TicketBO(string connectionKey) : base(connectionKey)
        {
            dao = new TicketDAO(connectionKey);
            bugDAO = new BugDAO(connectionKey);
            statusDAO = new StatusDAO(connectionKey);
            priorityDAO = new PriorityDAO(connectionKey);
            appDAO = new SoftwareAppDAO(connectionKey);
        }

        private void HydrateTicket(Ticket ticket)
        {
            Bug btemp = new Bug(ticket.GetBugId(), "%", DateTime.Now);
            ticket.Bug = (Bug)bugDAO.SelectOneObject(btemp);

            SoftwareApp aTemp = new SoftwareApp(ticket.GetSoftwareAppId(), "%", DateTime.Now);
            ticket.SoftwareApp = (SoftwareApp)appDAO.SelectOneObject(aTemp);

            Status sTemp = new Status(ticket.GetStatusId(), "%", DateTime.Now);
            if(ticket.GetStatusId() != -1)
            {
                ticket.Status = (Status)statusDAO.SelectOneObject(sTemp);
            }
            

            Priority pTemp = new Priority(ticket.GetPriorityId(), "%", DateTime.Now);
            if (ticket.GetPriorityId() != -1)
            {
                ticket.Priority = (Priority)priorityDAO.SelectOneObject(pTemp);
            }
        }

        public override object SelectOneObject(object filter)
        {
            Ticket ticket = (Ticket)dao.SelectOneObject(filter);
            HydrateTicket(ticket);

            return ticket;
        }

        public override IList<object> SelectManyObjects(object obj)
        {
            IList<object> tickets = dao.SelectManyObjects(obj);
            IList<object> lstResults = new List<object>();

            foreach(Ticket ticket in tickets)
            {
                HydrateTicket(ticket);
                lstResults.Add(ticket);
            }

            return lstResults;
        }

        public IList<object> SelectManyByBug(object filter)
        {
            IList<object> tickets = dao.SelectManyObjects(new Ticket(-1, "%", -1, -1, -1, -1, "%", DateTime.Now, "%"));
            IList<object> lstResults = new List<object>();

            foreach (Ticket ticket in tickets)
            {
                if (ticket.GetBugId() == ((Ticket)filter).GetBugId())
                {
                    HydrateTicket(ticket);
                    lstResults.Add(ticket);
                }
            }

            return lstResults;
        }

        public IList<object> SelectManyByStatus(object filter)
        {
            IList<object> tickets = dao.SelectManyObjects(new Ticket(-1, "%", -1, -1, -1, -1, "%", DateTime.Now, "%"));
            IList<object> lstResults = new List<object>();

            foreach(Ticket ticket in tickets)
            {
                if (ticket.GetStatusId() == ((Ticket)filter).GetStatusId())
                {
                    HydrateTicket(ticket);
                    lstResults.Add(ticket);
                }
            }

            return lstResults;
        }

        public IList<object> SelectManyByPriority(object filter)
        {
            IList<object> tickets = dao.SelectManyObjects(new Ticket(-1, "%", -1, -1, -1, -1, "%", DateTime.Now, "%"));
            IList<object> lstResults = new List<object>();

            foreach (Ticket ticket in tickets)
            {
                if (ticket.GetPriorityId() == ((Ticket)filter).GetPriorityId())
                {
                    HydrateTicket(ticket);
                    lstResults.Add(ticket);
                }
            }

            return lstResults;
        }

        public IList<object> SelectManyBySoftwareApp(object filter)
        {
            IList<object> tickets = dao.SelectManyObjects(new Ticket(-1, "%", -1, -1, -1, -1, "%", DateTime.Now, "%"));
            IList<object> lstResults = new List<object>();

            foreach (Ticket ticket in tickets)
            {
                if (ticket.GetSoftwareAppId() == ((Ticket)filter).GetSoftwareAppId())
                {
                    HydrateTicket(ticket);
                    lstResults.Add(ticket);
                }
            }

            return lstResults;
        }

        public IList<object> SelectManyByDescription(object filter)
        {
            IList<object> tickets = dao.SelectManyObjects(new Ticket(-1, "%", -1, -1, -1, -1, "%", DateTime.Now, "%"));
            IList<object> lstResults = new List<object>();

            foreach (Ticket ticket in tickets)
            {
                if (ticket.GetDescription().Contains(((Ticket)filter).GetDescription()))
                {
                    HydrateTicket(ticket);
                    lstResults.Add(ticket);
                }
            }

            return lstResults;
        }

        public IList<object> SelectManyByDateCreated(object filter)
        {
            IList<object> tickets = dao.SelectManyObjects(new Ticket(-1, "%", -1, -1, -1, -1, "%", DateTime.Now, "%"));
            IList<object> lstResults = new List<object>();

            foreach (Ticket ticket in tickets)
            {
                if (ticket.GetDateCreated() == ((Ticket)filter).GetDateCreated())
                {
                    HydrateTicket(ticket);
                    lstResults.Add(ticket);
                }
            }

            return lstResults;
        }

        public IList<object> SelectManyByDateResolved(object filter)
        {
            IList<object> tickets = dao.SelectManyObjects(new Ticket(-1, "%", -1, -1, -1, -1, "%", DateTime.Now, "%"));
            IList<object> lstResults = new List<object>();

            foreach (Ticket ticket in tickets)
            {
                if (ticket.GetDateResolved() == ((Ticket)filter).GetDateResolved())
                {
                    HydrateTicket(ticket);
                    lstResults.Add(ticket);
                }
            }

            return lstResults;
        }








        public override object InsertOneObject(object obj)
        {
            return dao.InsertOneObject(obj);
        }

        public override object UpdateOneObject(object obj)
        {
            return dao.UpdateOneObject(obj);
        }

        public override object DeleteOneObject(object obj)
        {
            return dao.DeleteOneObject(obj);
        }

        public override void InsertManyObjects()
        {

        }

        public override void UpdateManyObjects()
        {

        }

        public override void DeleteManyObjects()
        {

        }
    }
}
