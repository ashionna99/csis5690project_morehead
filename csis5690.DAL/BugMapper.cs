﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csis5690.Domain;

namespace csis5690.DAL
{
    public class BugMapper : BaseMapper
    {
        public BugMapper(SqlDataReader rdr) : base(rdr)
        {

        }

        public override object DoMapping()
        {
            logger.Debug("Insdie BugMapper DoMapping()!!!");

            int id = GetInteger("id");
            string name = GetString("name");
            DateTime dateCreated = GetDateTime("date_created");

            Bug rtnObj = new Bug(id, name, dateCreated);
            logger.Debug($"Inside BugMapper DoMapping() {rtnObj.ToString()}");

            return rtnObj;
        }
    }
}
