﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csis5690.DAL
{
    public class DALException : Exception
    {
        public DALException() : this("An error occured in the Data Access Layer.")
        {

        }

        public DALException (string message) : base (message)
        {

        }
    }
}
