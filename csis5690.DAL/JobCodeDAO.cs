﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csis5690.Domain;

namespace csis5690.DAL
{
    public class JobCodeDAO : BaseDAO
    {
        //sql
        protected string selectOneSql = "SELECT ID, DESCRIPTION FROM JOBCODE WHERE ID = @idParm;";
        protected string selectManySql = "SELECT ID, DESCRIPTION FROM JOBCODE WHERE DESCRIPTION LIKE @descriptionParm;";
        protected string insertOneSql = "INSERT INTO JOBCODE (DESCRIPTION) VALUES (@descriptionParm); SELECT SCOPE_IDENTITY();";
        protected string updateOneSql = "UPDATE JOBCODE SET DESCRIPTION = @descriptionParm WHERE ID = @idParm;";
        protected string deleteOneSql = "DELETE FROM JOBCODE WHERE ID = @idParm;";

        //constructor
        public JobCodeDAO(string connectionKey):base(connectionKey)
        {

        }

        public override object SelectOneObject(object obj)
        {
            try
            {
                JobCode filter = (JobCode)obj;
                JobCode rtnObj = null;
                int id;
                string description;

                logger.Debug("Inside SelectOneObject!!!");

                conn = new SqlConnection(connString);
                conn.Open();

                sql = selectOneSql;

                cmd = new SqlCommand(sql, conn);

                SqlParameter idParm = new SqlParameter();
                idParm.ParameterName = "@idParm";
                idParm.Value = filter.GetId();

                cmd.Parameters.Add(idParm);

                rdr = cmd.ExecuteReader();
                JobCodeMapper mapper = new JobCodeMapper(rdr);

                while(rdr.Read())
                {
                    rtnObj = (JobCode)mapper.DoMapping();
                }

                logger.Debug(rtnObj.ToString());

                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override IList<object> SelectManyObjects(object obj)
        {
            try
            {
                IList<object> objList = new List<object>();
                JobCode filter = (JobCode)obj;
                JobCode rtnObj = null;

                logger.Debug("Inside SelectManyObjects!!!");

                conn = new SqlConnection(connString);
                conn.Open();

                sql = selectManySql;

                cmd = new SqlCommand(sql, conn);

                SqlParameter descriptionParm = new SqlParameter();
                descriptionParm.ParameterName = "@descriptionParm";
                descriptionParm.Value = '%' + filter.GetDescription() + '%';

                cmd.Parameters.Add(descriptionParm);

                rdr = cmd.ExecuteReader();
                JobCodeMapper mapper = new JobCodeMapper(rdr);

                while(rdr.Read())
                {
                    rtnObj = (JobCode)mapper.DoMapping();

                    logger.Debug($"Getting from DB: {rtnObj.ToString()}");
                    objList.Add(rtnObj);
                }

                return objList;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override object InsertOneObject(object obj)
        {
            try
            {
                JobCode rtnObj = (JobCode)obj;
                logger.Debug(rtnObj.ToString());

                int id;

                logger.Debug("Inside InsertOneObject!!!");

                conn = new SqlConnection(connString);
                adpt = new SqlDataAdapter();

                sql = insertOneSql;

                SqlParameter descriptionParm = new SqlParameter();
                descriptionParm.ParameterName = "@descriptionParm";
                descriptionParm.Value = rtnObj.GetDescription();

                conn.Open();
                adpt.InsertCommand = new SqlCommand(sql, conn);
                adpt.InsertCommand.Parameters.Add(descriptionParm);

                id = Convert.ToInt32(adpt.InsertCommand.ExecuteScalar());
                rtnObj.SetId(id);

                logger.Debug(rtnObj.ToString());
                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override object UpdateOneObject(object obj)
        {
            try
            {
                JobCode rtnObj = (JobCode)obj;
                logger.Debug(rtnObj.ToString());
                logger.Debug("Inside UpdateOneObject!!!");

                conn = new SqlConnection(connString);
                adpt = new SqlDataAdapter();

                sql = updateOneSql;

                SqlParameter idParm = new SqlParameter();
                idParm.ParameterName = "@idParm";
                idParm.Value = rtnObj.GetId();

                SqlParameter descriptionParm = new SqlParameter();
                descriptionParm.ParameterName = "@descriptionParm";
                descriptionParm.Value = rtnObj.GetDescription();

                conn.Open();
                adpt.UpdateCommand = new SqlCommand(sql, conn);
                adpt.UpdateCommand.Parameters.Add(idParm);
                adpt.UpdateCommand.Parameters.Add(descriptionParm);

                adpt.UpdateCommand.ExecuteNonQuery();

                logger.Debug(rtnObj.ToString());
                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override object DeleteOneObject(object obj)
        {
            try
            {
                JobCode rtnObj = (JobCode)obj;
                logger.Debug(rtnObj.ToString());
                logger.Debug("Inside DeleteOneObject!!!");

                conn = new SqlConnection(connString);
                adpt = new SqlDataAdapter();

                sql = deleteOneSql;

                SqlParameter idParm = new SqlParameter();
                idParm.ParameterName = "@idParm";
                idParm.Value = rtnObj.GetId();

                conn.Open();
                adpt.DeleteCommand = new SqlCommand(sql, conn);
                adpt.DeleteCommand.Parameters.Add(idParm);

                adpt.DeleteCommand.ExecuteNonQuery();

                logger.Debug(rtnObj.ToString());
                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }





        public override void InsertManyObjects()
        {
            throw new NotImplementedException();
        }
        public override void UpdateManyObjects()
        {
            throw new NotImplementedException();
        }

        public override void DeleteManyObjects()
        {
            throw new NotImplementedException();
        }
    }
}
