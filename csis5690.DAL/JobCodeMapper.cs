﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csis5690.Domain;

namespace csis5690.DAL
{
    public class JobCodeMapper : BaseMapper
    {
        public JobCodeMapper(SqlDataReader rdr) : base(rdr)
        {

        }

        public override object DoMapping()
        {
            logger.Debug("Inside JobCodeMapper DoMapping()!!!");
            int id = GetInteger("id");
            string description = GetString("description");

            JobCode code = new JobCode(id, description);
            logger.Debug($"Inside JobCodeMapper DoMapping() {code.ToString()}");

            return code;
        }
    }
}
