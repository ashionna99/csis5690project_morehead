﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csis5690.Domain;

namespace csis5690.DAL
{
    public class PriorityMapper : BaseMapper
    {
        public PriorityMapper(SqlDataReader rdr) : base(rdr)
        {

        }

        public override object DoMapping()
        {
            logger.Debug("Inside PriorityMapper DoMapping()!!!");

            int id = GetInteger("id");
            string name = GetString("name");
            DateTime dateCreated = GetDateTime("date_created");

            Priority rtnObj = new Priority(id, name, dateCreated);
            logger.Debug($"Inside PriorityMapper DoMapping() {rtnObj.ToString()}");

            return rtnObj;
        }
    }
}
