﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csis5690.Domain;
using log4net;

namespace csis5690.DAL
{
    public class SoftwareAppMapper : BaseMapper
    {
        public SoftwareAppMapper(SqlDataReader rdr) : base(rdr)
        {

        }

        public override object DoMapping()
        {
            logger.Debug("Inside SoftwareAppMapper DoMapping().");

            int id = GetInteger("id");
            string name = GetString("name");
            DateTime dateCreated = GetDateTime("date_created");

            SoftwareApp rtnObj = new SoftwareApp(id, name, dateCreated);
            logger.Debug($"Inside SoftwareAppMapper DoMapping().");

            return rtnObj;
        }
    }
}
