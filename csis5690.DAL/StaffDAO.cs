﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csis5690.Domain;

namespace csis5690.DAL
{
    public class StaffDAO : BaseDAO
    {
        //sql
        protected string selectOneSql = "SELECT ID, USERNAME, PASSWORD, JOBCODE FROM STAFF WHERE ID = @idParm;";
        protected string selectManySql = "SELECT ID, USERNAME, PASSWORD, JOBCODE FROM STAFF WHERE USERNAME LIKE @usernameParm;";
        protected string selectManyWithJobcode = "SELECT s.Id, s.Username, s.Password, s.Jobcode, j.Description FROM STAFF s JOIN Jobcode j ON s.Jobcode = j.Id;";
        protected string insertOneSql = "INSERT INTO STAFF (USERNAME, PASSWORD, JOBCODE) VALUES (@usernameParm, @passwordParm, @jobCodeParm); SELECT SCOPE_IDENTITY();";
        protected string updateOneSql = "UPDATE STAFF SET USERNAME = @usernameParm, PASSWORD = @passwordParm, JOBCODE = @jobCodeParm WHERE ID = @idParm;";
        protected string deleteOneSql = "DELETE FROM STAFF WHERE ID = @idParm;";

        //constructor
        public StaffDAO(string connectionKey) : base(connectionKey)
        {

        }

        public override object SelectOneObject(object obj)
        {
            try
            {
                Staff filter = (Staff)obj;
                Staff rtnObj = null;
                int id;
                string username;
                string password;

                logger.Debug("Inside SelectOneObject!!!");

                conn = new SqlConnection(connString);
                conn.Open();

                sql = selectOneSql;

                cmd = new SqlCommand(sql, conn);

                SqlParameter idParm = new SqlParameter();
                idParm.ParameterName = "@idParm";
                idParm.Value = filter.GetId();

                cmd.Parameters.Add(idParm);

                rdr = cmd.ExecuteReader();
                StaffMapper mapper = new StaffMapper(rdr);

                while(rdr.Read())
                {
                    rtnObj = (Staff)mapper.DoMapping();
                }

                logger.Debug(rtnObj.ToString());

                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override IList<object> SelectManyObjects(object obj)
        {
            try
            {
                IList<object> objList = new List<object>();
                Staff filter = (Staff)obj;
                Staff rtnObj = null;
                int id;
                string username;
                string password;

                logger.Debug("Inside SelectManyObjects!!!");

                conn = new SqlConnection(connString);
                conn.Open();

                sql = selectManySql;

                cmd = new SqlCommand(sql, conn);

                SqlParameter usernameParm = new SqlParameter();
                usernameParm.ParameterName = "@usernameParm";
                usernameParm.Value = '%' + filter.GetUsername() + '%';

                SqlParameter passwordParm = new SqlParameter();
                passwordParm.ParameterName = "@passwordParm";
                passwordParm.Value = '%' + filter.GetPassword() + '%';

                SqlParameter jobCodeParm = new SqlParameter();
                jobCodeParm.ParameterName = "@jobCodeParm";
                jobCodeParm.Value = filter.GetJobCode();

                cmd.Parameters.Add(usernameParm);
                cmd.Parameters.Add(passwordParm);
                cmd.Parameters.Add(jobCodeParm);

                rdr = cmd.ExecuteReader();
                StaffMapper mapper = new StaffMapper(rdr);

                while (rdr.Read())
                {
                    rtnObj = (Staff)mapper.DoMapping();

                    logger.Debug($"Getting from DB: {rtnObj.ToString()}");
                    objList.Add(rtnObj);
                }
                return objList;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public DataSet SelectManyWithJobCode()
        {
            try
            {
                DataSet dataSet = new DataSet();

                logger.Debug("Inside SelectManyWithJobCode!!!");

                conn = new SqlConnection(connString);

                sql = selectManyWithJobcode;

                adpt = new SqlDataAdapter(sql, conn);

                conn.Open();

                adpt.Fill(dataSet);

                return dataSet;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override object InsertOneObject(object obj)
        {
            try
            {
                Staff rtnObj = (Staff)obj;
                logger.Debug(rtnObj.ToString());

                int id;

                logger.Debug("Inside InserOneObject!!!");

                conn = new SqlConnection(connString);
                adpt = new SqlDataAdapter();

                sql = insertOneSql;

                SqlParameter usernameParm = new SqlParameter();
                usernameParm.ParameterName = "@usernameParm";
                usernameParm.Value = rtnObj.GetUsername();

                SqlParameter passwordParm = new SqlParameter();
                passwordParm.ParameterName = "@passwordParm";
                passwordParm.Value = rtnObj.GetPassword();

                SqlParameter jobCodeParm = new SqlParameter();
                jobCodeParm.ParameterName = "@jobCodeParm";
                jobCodeParm.Value = rtnObj.GetJobCode();

                conn.Open();
                adpt.InsertCommand = new SqlCommand(sql, conn);
                adpt.InsertCommand.Parameters.Add(usernameParm);
                adpt.InsertCommand.Parameters.Add(passwordParm);
                adpt.InsertCommand.Parameters.Add(jobCodeParm);

                id = Convert.ToInt32(adpt.InsertCommand.ExecuteScalar());
                rtnObj.SetId(id);

                logger.Debug(rtnObj.ToString());
                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override object UpdateOneObject(object obj)
        {
            try
            {
                Staff rtnObj = (Staff)obj;
                logger.Debug(rtnObj.ToString());
                logger.Debug("Inside UpdateOneObject!!!");

                conn = new SqlConnection(connString);
                adpt = new SqlDataAdapter();

                sql = updateOneSql;
                
                SqlParameter idParm = new SqlParameter();
                idParm.ParameterName = "@idParm";
                idParm.Value = rtnObj.GetId();

                SqlParameter usernameParm = new SqlParameter();
                usernameParm.ParameterName = "@usernameParm";
                usernameParm.Value = rtnObj.GetUsername();

                SqlParameter passwordParm = new SqlParameter();
                passwordParm.ParameterName = "@passwordParm";
                passwordParm.Value = rtnObj.GetPassword();

                SqlParameter jobCodeParm = new SqlParameter();
                jobCodeParm.ParameterName = "@jobCodeParm";
                jobCodeParm.Value = rtnObj.GetJobCode();

                conn.Open();
                adpt.UpdateCommand = new SqlCommand(sql, conn);
                adpt.UpdateCommand.Parameters.Add(idParm);
                adpt.UpdateCommand.Parameters.Add(usernameParm);
                adpt.UpdateCommand.Parameters.Add(passwordParm);
                adpt.UpdateCommand.Parameters.Add(jobCodeParm);

                adpt.UpdateCommand.ExecuteNonQuery();

                logger.Debug(rtnObj.ToString());
                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override object DeleteOneObject(object obj)
        {
            try
            {
                Staff rtnObj = (Staff)obj;
                logger.Debug(rtnObj.ToString());
                logger.Debug("Inside DeleteOneObject!!!");

                conn = new SqlConnection(connString);
                adpt = new SqlDataAdapter();

                sql = deleteOneSql;

                SqlParameter idParm = new SqlParameter();
                idParm.ParameterName = "@idParm";
                idParm.Value = rtnObj.GetId();

                conn.Open();
                adpt.DeleteCommand = new SqlCommand(sql, conn);
                adpt.DeleteCommand.Parameters.Add(idParm);

                adpt.DeleteCommand.ExecuteNonQuery();

                logger.Debug(rtnObj.ToString());
                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }



       

        public override void InsertManyObjects()
        {
            throw new NotImplementedException();
        }

        public override void UpdateManyObjects()
        {
            throw new NotImplementedException();
        }

        public override void DeleteManyObjects()
        {
            throw new NotImplementedException();
        }
    }
}
