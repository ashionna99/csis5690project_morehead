﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csis5690.Domain;

namespace csis5690.DAL
{
    public class StaffMapper : BaseMapper
    {
        public StaffMapper(SqlDataReader rdr) : base(rdr)
        {

        }

        public override object DoMapping()
        {
            logger.Debug("Inside StaffMapper DoMapping()!!!");

            int id = GetInteger("id");
            string username = GetString("username");
            string password = GetString("password");
            int jobCode = GetInteger("jobcode");

            Staff rtnObj = new Staff(id, username, password, jobCode);
            logger.Debug($"Inside StaffMapper DoMapping() {rtnObj.ToString()}");

            return rtnObj;
        }
    }
}
