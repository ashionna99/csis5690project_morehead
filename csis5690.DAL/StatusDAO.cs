﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csis5690.Domain;

namespace csis5690.DAL
{
    public class StatusDAO : BaseDAO
    {
        //sql
        protected string selectOneSql = "SELECT ID, NAME, DATE_CREATED FROM STATUS WHERE ID = @idParm;";
        protected string selectManySql = "SELECT ID, NAME, DATE_CREATED FROM STATUS WHERE NAME LIKE @nameParm;";
        protected string insertOneSql = "INSERT INTO STATUS (NAME) VALUES (@nameParm); SELECT SCOPE_IDENTITY();";
        protected string updateOneSql = "UPDATE STATUS SET NAME = @nameParm WHERE ID = @idParm;";
        protected string deleteOneSql = "DELETE FROM STATUS WHERE ID = @idParm;";

        //constrctor
        public StatusDAO(string connectionKey) : base(connectionKey)
        {

        }

        public override object SelectOneObject(object obj)
        {
            try
            {
                Status filter = (Status)obj;
                Status rtnObj = null;
                int id;
                string name;
                DateTime dateTime;

                logger.Debug("Inside SelectOneObject!!!");

                conn = new SqlConnection(connString);
                conn.Open();

                sql = selectOneSql;

                cmd = new SqlCommand(sql, conn);

                SqlParameter idParm = new SqlParameter();
                idParm.ParameterName = "@idParm";
                idParm.Value = filter.GetId();

                cmd.Parameters.Add(idParm);

                rdr = cmd.ExecuteReader();
                StatusMapper mapper = new StatusMapper(rdr);

                while (rdr.Read())
                {
                    rtnObj = (Status)mapper.DoMapping();
                }

                logger.Debug(rtnObj.ToString());

                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override IList<object> SelectManyObjects(object obj)
        {
            try
            {
                IList<object> objList = new List<object>();
                Status filter = (Status)obj;
                Status rtnObj = null;
                int id;
                string name;
                DateTime dateTime;

                logger.Debug("Inside SelectManyObjects!!!");

                conn = new SqlConnection(connString);
                conn.Open();

                sql = selectManySql;

                cmd = new SqlCommand(sql, conn);

                SqlParameter nameParm = new SqlParameter();
                nameParm.ParameterName = "@nameParm";
                nameParm.Value = '%' + filter.GetName() + '%';

                cmd.Parameters.Add(nameParm);

                rdr = cmd.ExecuteReader();
                StatusMapper mapper = new StatusMapper(rdr);

                while (rdr.Read())
                {
                    rtnObj = (Status)mapper.DoMapping();

                    logger.Debug($"Getting from DB: {rtnObj.ToString()}");
                    objList.Add(rtnObj);
                }
                return objList;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override object InsertOneObject(object obj)
        {
            try
            {
                Status rtnObj = (Status)obj;
                logger.Debug(rtnObj.ToString());

                int id;

                logger.Debug("Inside InsertOneObject!!!");

                conn = new SqlConnection(connString);
                adpt = new SqlDataAdapter();

                sql = insertOneSql;

                SqlParameter nameParm = new SqlParameter();
                nameParm.ParameterName = "@nameParm";
                nameParm.Value = rtnObj.GetName();

                conn.Open();
                adpt.InsertCommand = new SqlCommand(sql, conn);
                adpt.InsertCommand.Parameters.Add(nameParm);

                id = Convert.ToInt32(adpt.InsertCommand.ExecuteScalar());
                rtnObj.SetId(id);

                logger.Debug(rtnObj.ToString());
                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override object UpdateOneObject(object obj)
        {
            try
            {
                Status rtnObj = (Status)obj;
                logger.Debug(rtnObj.ToString());
                logger.Debug("Inside UpdateOneObject!!!");

                conn = new SqlConnection(connString);
                adpt = new SqlDataAdapter();

                sql = updateOneSql;

                SqlParameter nameParm = new SqlParameter();
                nameParm.ParameterName = "@nameParm";
                nameParm.Value = rtnObj.GetName();

                SqlParameter idParm = new SqlParameter();
                idParm.ParameterName = "@idParm";
                idParm.Value = rtnObj.GetId();

                conn.Open();
                adpt.UpdateCommand = new SqlCommand(sql, conn);
                adpt.UpdateCommand.Parameters.Add(nameParm);
                adpt.UpdateCommand.Parameters.Add(idParm);

                adpt.UpdateCommand.ExecuteNonQuery();

                logger.Debug(rtnObj.ToString());
                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override object DeleteOneObject(object obj)
        {
            try
            {
                Status rtnObj = (Status)obj;
                logger.Debug(rtnObj.ToString());
                logger.Debug("Inside DeleteOneObject!!!");

                conn = new SqlConnection(connString);
                adpt = new SqlDataAdapter();

                sql = deleteOneSql;

                SqlParameter idParm = new SqlParameter();
                idParm.ParameterName = "@idParm";
                idParm.Value = rtnObj.GetId();

                conn.Open();
                adpt.DeleteCommand = new SqlCommand(sql, conn);
                adpt.DeleteCommand.Parameters.Add(idParm);

                adpt.DeleteCommand.ExecuteNonQuery();

                logger.Debug(rtnObj.ToString());
                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }



        public override void InsertManyObjects()
        {
            throw new NotImplementedException();
        }

        public override void UpdateManyObjects()
        {
            throw new NotImplementedException();
        }

        public override void DeleteManyObjects()
        {
            throw new NotImplementedException();
        }
    }
}
