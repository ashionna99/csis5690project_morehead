﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csis5690.Domain;

namespace csis5690.DAL
{
    public class StatusMapper : BaseMapper
    {
        public StatusMapper(SqlDataReader rdr) : base(rdr)
        {

        }

        public override object DoMapping()
        {
            logger.Debug("Inside StatusMapper DoMapping()!!!");

            int id = GetInteger("id");
            string name = GetString("name");
            DateTime dateCreated = GetDateTime("date_created");

            Status rtnObj = new Status(id, name, dateCreated);
            logger.Debug($"Inside StatusMapper DoMapping() {rtnObj.ToString()}");

            return rtnObj;
        }
    }
}
