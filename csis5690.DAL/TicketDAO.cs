﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using csis5690.Domain;

namespace csis5690.DAL
{
    public class TicketDAO : BaseDAO
    {
        //sql
        protected string selectOneSql = "SELECT ID, DESCRIPTION, BUG_ID, SOFTWARE_APP_ID, PRIORITY_ID, STATUS_ID, RESOLUTION, DATE_CREATED, DATE_RESOLVED FROM TICKET WHERE ID = @idParm;";
        protected string selectManySql = "SELECT ID, DESCRIPTION, BUG_ID, SOFTWARE_APP_ID, PRIORITY_ID, STATUS_ID, RESOLUTION, DATE_CREATED, DATE_RESOLVED FROM TICKET WHERE DESCRIPTION LIKE @descriptionParm";
        protected string insertOneSql = "INSERT INTO TICKET (DESCRIPTION, BUG_ID, SOFTWARE_APP_ID, PRIORITY_ID)" + 
                                        "VALUES (@descriptionParm, @bugIdParm, @softwareAppidParm, @priorityIdParm); SELECT SCOPE_IDENTITY();";
        protected string updateOneSql = "UPDATE TICKET SET DESCRIPTION = @descriptionParm" +
                                                ", BUG_ID = @bugIdParm" + 
                                                ", SOFTWARE_APP_ID = @softwareAppidParm" + 
                                                ", PRIORITY_ID = @priorityIdParm" + 
                                                ", STATUS_ID = @statusIdParm" + 
                                                ", RESOLUTION = @resolutionParm" + 
                                                ", DATE_RESOLVED = @dateResolvedParm" + 
                                                " WHERE ID = @idParm;";
        protected string deleteOneSql = "DELETE FROM TICKET WHERE ID = @idParm";


        //constructor
        public TicketDAO(string connectionKey) : base(connectionKey)
        {

        }

        public override object SelectOneObject(object obj)
        {
            try
            {
                Ticket filter = (Ticket)obj;
                Ticket rtnObj = null;
                int id;
                string description;
                int bugId;
                int appId;
                int priorityId;
                int statusId;
                string resolution;
                DateTime dateCreated;
                DateTime dateResolved;

                logger.Debug("Inside SelectOneObject!!!");

                conn = new SqlConnection(connString);
                conn.Open();

                sql = selectOneSql;

                cmd = new SqlCommand(sql, conn);

                SqlParameter idParm = new SqlParameter();
                idParm.ParameterName = "@idParm";
                idParm.Value = filter.GetId();

                cmd.Parameters.Add(idParm);

                rdr = cmd.ExecuteReader();
                TicketMapper mapper = new TicketMapper(rdr);

                while(rdr.Read())
                {
                    rtnObj = (Ticket)mapper.DoMapping();
                }

                return rtnObj;

            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override IList<object> SelectManyObjects(object obj)
        {
            try
            {
                IList<object> objList = new List<object>();
                Ticket filter = (Ticket)obj;
                Ticket rtnObj = null;
                int id;
                string description;
                int bugId;
                int appId;
                int priorityId;
                int statusId;
                string resolution;
                DateTime dateTime;
                DateTime dateResolved;

                logger.Debug("Inside SelectManyObjects!!!");

                conn = new SqlConnection(connString);
                conn.Open();

                sql = selectManySql;

                cmd = new SqlCommand(sql, conn);

                SqlParameter descriptionParm = new SqlParameter();
                descriptionParm.ParameterName = "@descriptionParm";
                descriptionParm.Value = '%' + filter.GetDescription() + '%';

                cmd.Parameters.Add(descriptionParm);

                rdr = cmd.ExecuteReader(); 
                TicketMapper mapper = new TicketMapper(rdr);

                while (rdr.Read())
                {
                    rtnObj = (Ticket)mapper.DoMapping();

                    logger.Debug($"Getting from database: {rtnObj.ToString()}");
                    objList.Add(rtnObj);
                }

                return objList;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override object InsertOneObject(object obj)
        {
            try
            {
                Ticket rtnObj = (Ticket)obj;
                logger.Debug(rtnObj.ToString());

                int id;

                logger.Debug("Inside InsertOneObject!!!");

                conn = new SqlConnection(connString);
                adpt = new SqlDataAdapter();

                sql = insertOneSql;

                SqlParameter descriptionParm = new SqlParameter();
                descriptionParm.ParameterName = "@descriptionParm";
                descriptionParm.Value = rtnObj.GetDescription();

                SqlParameter bugIdParm = new SqlParameter();
                bugIdParm.ParameterName = "@bugIdParm";
                bugIdParm.Value = rtnObj.GetBugId();

                SqlParameter softwareAppidParm = new SqlParameter();
                softwareAppidParm.ParameterName = "@softwareAppidParm";
                softwareAppidParm.Value = rtnObj.GetSoftwareAppId();

                SqlParameter priorityIdParm = new SqlParameter();
                priorityIdParm.ParameterName = "@priorityIdParm";
                priorityIdParm.Value = rtnObj.GetPriorityId();

                conn.Open();
                adpt.InsertCommand = new SqlCommand(sql, conn);
                adpt.InsertCommand.Parameters.Add(descriptionParm);
                adpt.InsertCommand.Parameters.Add(bugIdParm);
                adpt.InsertCommand.Parameters.Add(softwareAppidParm);
                adpt.InsertCommand.Parameters.Add(priorityIdParm);
          

                id = Convert.ToInt32(adpt.InsertCommand.ExecuteScalar());
                rtnObj.SetId(id);

                logger.Debug(rtnObj.ToString());
                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override object UpdateOneObject(object obj)
        {
            try
            {
                Ticket rtnObj = (Ticket)obj;
                logger.Debug(rtnObj.ToString());
                logger.Debug("Inside UpdateOneObject!!!");

                conn = new SqlConnection(connString);
                adpt = new SqlDataAdapter();

                sql = updateOneSql;

                SqlParameter descriptionParm = new SqlParameter();
                descriptionParm.ParameterName = "@descriptionParm";
                descriptionParm.Value = rtnObj.GetDescription();

                SqlParameter bugIdParm = new SqlParameter();
                bugIdParm.ParameterName = "@bugIdParm";
                bugIdParm.Value = rtnObj.GetBugId();

                SqlParameter softwareAppidParm = new SqlParameter();
                softwareAppidParm.ParameterName = "@softwareAppidParm";
                softwareAppidParm.Value = rtnObj.GetSoftwareAppId();

                SqlParameter priorityIdParm = new SqlParameter();
                priorityIdParm.ParameterName = "@priorityIdParm";
                priorityIdParm.Value = rtnObj.GetPriorityId();

                SqlParameter statusIdParm = new SqlParameter();
                statusIdParm.ParameterName = "@statusIdParm";
                statusIdParm.Value = rtnObj.GetStatusId();

                SqlParameter resolutionParm = new SqlParameter();
                resolutionParm.ParameterName = "@resolutionParm";
                resolutionParm.Value = rtnObj.GetResolution();

                SqlParameter dateResolvedParm = new SqlParameter();
                dateResolvedParm.ParameterName = "@dateResolvedParm";
                dateResolvedParm.Value = rtnObj.GetDateResolved();

                SqlParameter idParm = new SqlParameter();
                idParm.ParameterName = "@idParm";
                idParm.Value = rtnObj.GetId();

                conn.Open();
                adpt.UpdateCommand = new SqlCommand(sql, conn);
                adpt.UpdateCommand.Parameters.Add(descriptionParm);
                adpt.UpdateCommand.Parameters.Add(bugIdParm);
                adpt.UpdateCommand.Parameters.Add(softwareAppidParm);
                adpt.UpdateCommand.Parameters.Add(priorityIdParm);
                adpt.UpdateCommand.Parameters.Add(statusIdParm);
                adpt.UpdateCommand.Parameters.Add(resolutionParm);
                adpt.UpdateCommand.Parameters.Add(dateResolvedParm);
                adpt.UpdateCommand.Parameters.Add(idParm);

                adpt.UpdateCommand.ExecuteNonQuery(); 

                logger.Debug(rtnObj.ToString());
                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }

        public override object DeleteOneObject(object obj)
        {
            try
            {
                Ticket rtnObj = (Ticket)obj;
                logger.Debug(rtnObj.ToString());
                logger.Debug("Inside DeleteOneObject!!!");

                conn = new SqlConnection(connString);
                adpt = new SqlDataAdapter();

                sql = deleteOneSql;

                SqlParameter idParm = new SqlParameter();
                idParm.ParameterName = "@idParm";
                idParm.Value = rtnObj.GetId();

                conn.Open();
                adpt.DeleteCommand = new SqlCommand(sql, conn);
                adpt.DeleteCommand.Parameters.Add(idParm);

                adpt.DeleteCommand.ExecuteNonQuery();

                logger.Debug(rtnObj.ToString());
                return rtnObj;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            finally
            {
                CleanUp();
            }
        }



        public override void InsertManyObjects()
        {
            throw new NotImplementedException();
        }

        public override void UpdateManyObjects()
        {
            throw new NotImplementedException();
        }

        public override void DeleteManyObjects()
        {
            throw new NotImplementedException();
        }

    }
}
