﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csis5690.Domain;

namespace csis5690.DAL
{
    public class TicketMapper : BaseMapper
    {
        public TicketMapper(SqlDataReader rdr) : base(rdr)
        {

        }

        public override object DoMapping()
        {
            logger.Debug("Inside TicketMapper DoMapping()!!!");

            int id = GetInteger("id");
            string description = GetString("description");
            int bugId = GetInteger("bug_Id");
            int appId = GetInteger("software_app_id");
            int priorityId = GetInteger("priority_id");
            int statusId = GetInteger("status_id");
            string resolution = GetString("resolution");
            DateTime dateCreated = GetDateTime("date_created");
            string dateResolved = GetString("date_resolved");

            Ticket rtnObj = new Ticket(id, description, bugId, appId, priorityId, statusId, resolution, dateCreated, dateResolved);
            logger.Debug($"Inside TicketMapper DoMapping() {rtnObj.ToString()}");

            return rtnObj;
        }
    }
}
