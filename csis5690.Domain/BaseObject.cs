﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace csis5690.Domain
{
    public abstract class BaseObject
    {
        //member variables
        protected int id;
        protected DateTime dateCreated;

        //constructor
        public BaseObject (int Id, DateTime dateCreated)
        {
            SetId(Id);
            SetDateCreated(dateCreated);
        }

        //setters
        public void SetId(int id)
        {
            this.id = id;
        }

        public void SetDateCreated(DateTime dateCreated)
        {
            this.dateCreated = dateCreated;
        }

        //getters
        public int GetId()
        {
            return id;
        }

        public DateTime GetDateCreated()
        {
            return dateCreated;
        }

        //tostring display
        public override string ToString()
        {
            return $"BASE:  ID: {id}    DTC: {dateCreated}";
        }
    }
}
