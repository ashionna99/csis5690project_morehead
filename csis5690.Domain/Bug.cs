﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace csis5690.Domain
{
    public class Bug : BaseObject
    {
        //private member variables
        protected string name;

        //properties
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        //constructor
        public Bug(int Id, string issue, DateTime dateCreated) : base (Id, dateCreated)
        {
            SetName(issue);
        }

        //setters
        public void SetName(string name)
        {
            if(name.Trim().Length <= 0)
            {
                throw new DomainException("Bug name cannot be blank.");
            }
            else
            {
                this.name = name;
            }
        }

        //getters
        public string GetName()
        {
            return name;
        }


        //tostring display
        public override string ToString()
        {
            return $"BUG:  ID: {Id}   NM: {name}   DTC: {dateCreated}";
        }
    }
}
