﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csis5690.Domain
{
    public class DomainException : Exception
    {
        public DomainException() : base ("An exception occured in the Domain layer.")
        {

        }

        public DomainException(string message) : base (message)
        {

        }
    }
}
