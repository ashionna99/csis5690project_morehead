﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace csis5690.Domain
{
    public class JobCode
    {
        //member variables
        protected int id;
        protected string description;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        //constructor
        public JobCode(int id, string description)
        {
            SetId(id);
            SetDescription(description);
        }

        //setters
        public void SetId(int id)
        {
            this.id = id;
        }

        public void SetDescription(string description)
        {
            if(description.Trim().Length <= 0)
            {
                throw new DomainException("Job Code description cannot be blank.");
            }
            this.description = description;
        }

        //getters
        public int GetId()
        {
            return id;
        }

        public string GetDescription()
        {
            return description;
        }

        //tostring
        public override string ToString()
        {
            return $"JobCode: ID{id}  Description: {description}";
        }

    }
}
