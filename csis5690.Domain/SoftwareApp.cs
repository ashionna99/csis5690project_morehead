﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace csis5690.Domain
{
    public class SoftwareApp : BaseObject
    {
        //private member variables
        private string name;

        //properties
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        //constructor
        public SoftwareApp(int Id, string Name, DateTime dateCreated) : base(Id, dateCreated)
        {
            SetName(Name);
        }

        //setters
        public void SetName(string name)
        {
            if (name.Trim().Length <= 0)
            {
                throw new DomainException("Software App name cannot be blank.");
            }
            this.name = name;
        }

        //getters
        public string GetName()
        {
            return name;
        }

        //tostring 
        public override string ToString()
        {
            return $"SOFTWARE APP: ID: {Id}    NM: {name}   DTC: {dateCreated}";
        }
    }
}
