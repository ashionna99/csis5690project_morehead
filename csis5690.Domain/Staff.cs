﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace csis5690.Domain
{
    public class Staff
    {
        //member variables
        protected int id;
        protected string username;
        protected string password;
        protected int jobcode;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public int JobCode
        {
            get { return jobcode; }
            set { jobcode = value; }
        }

        //constructor
        public Staff(int id, string username, string password, int jobCode)
        {
            SetId(id);
            SetUsername(username);
            SetPassword(password);
            SetJobCode(jobCode);
        }

        //setters
        public void SetId(int id)
        {
            this.id = id;
        }

        public void SetUsername(string username)
        {
            if (username.Trim().Length <= 0)
            {
                throw new DomainException("Username cannot be blank.");
            }
            this.username = username;
        }

        public void SetPassword(string password)
        {
            if (password.Trim().Length <= 0)
            {
                throw new DomainException("Password cannot be blank.");
            }
            this.password = password;
        }

        public void SetJobCode(int jobcode)
        {
            this.jobcode = jobcode;
        }

        //getters
        public int GetId()
        {
            return id;
        }

        public string GetUsername()
        {
            return username;
        }

        public string GetPassword()
        {
            return password;
        }

        public int GetJobCode()
        {
            return jobcode;
        }

        //tostring
        public override string ToString()
        {
            return $"STAFF: ID: {id}     USERNAME: {username}    PASS: {password}       CODE: {jobcode}";
        }
    }
}
