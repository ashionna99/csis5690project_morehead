﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace csis5690.Domain
{
    public class Ticket : BaseObject
    {
        //private member variables
        protected string descripton;
        protected Bug bug;
        protected SoftwareApp app;
        protected Priority priority;
        protected Status status;
        protected string resolution;
        protected string dateResolved;
        protected int bugId;
        protected int appId;
        protected int priorityId;
        protected int statusId;

        //properties
        public Bug Bug
        {
            get { return bug; }
            set { bug = value; }
        }

        public Priority Priority
        {
            get { return priority; }
            set { priority = value; }
        }

        public SoftwareApp SoftwareApp
        {
            get { return app; }
            set { app = value; }
        }

        public Status Status
        {
            get { return status; }
            set { status = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Description
        {
            get { return descripton; }
            set { descripton = value; }
        }

        public string BugName
        {
            get { return bug.Name; }
        }

        public string SoftwareAppName
        {
            get { return app.Name; }
        }

        public string PriorityName
        {
            get { return priority.Name; }
        }

        public string StatusName
        {
            get { return status.Name; }
        }

        public DateTime DateCreated
        {
            get { return dateCreated; }
            set { dateCreated = value; }
        }

        public string DateResolved
        {
            get { return dateResolved; }
            set { dateResolved = value; }
        }

        public string Resolution
        {
            get { return resolution; }
            set { resolution = value; }
        }

        //constructor
        public Ticket(int Id, string description, int bugId, int appId, int priorityId, int statusId, string resolution, DateTime dateCreated, string dateResolved) : base(Id, dateCreated)
        {
            SetDescription(description);
            SetBugId(bugId);
            SetSoftwareAppId(appId);
            SetPriorityId(priorityId);
            SetStatusId(statusId);
            SetResolution(resolution);
            SetDateResolved(dateResolved);
        }

        //setters
        public void SetDescription(string description)
        {
            if(description.Trim().Length <= 0)
            {
                throw new DomainException("Ticket description cannot be blank.");
            }
            else
            {
                this.descripton = description;
            }
        }

        public void SetBugId(int bugid)
        {
            this.bugId = bugid;
        }

        public void SetSoftwareAppId(int appId)
        {
            this.appId = appId;
        }

        public void SetPriorityId(int priorityId)
        {
            this.priorityId = priorityId;
        }

        public void SetStatusId(int statusId)
        {
            this.statusId = statusId;
        }

        public void SetResolution(string resolution)
        {
            this.resolution = resolution;
        }

        public void SetDateResolved(string dateResolved)
        {
            this.dateResolved = dateResolved;
        }

        //getters
        public string GetDescription()
        {
            return descripton;
        }

        public int GetBugId()
        {
            return bugId;
        }

        public int GetSoftwareAppId()
        {
            return appId;
        }

        public int GetPriorityId()
        {
            return priorityId;
        }

        public int GetStatusId()
        {
            return statusId;
        }

        public string GetResolution()
        {
            return resolution;
        }

        public string GetDateResolved()
        {
            return dateResolved;
        }

        //tostring
        public override string ToString()
        {
            return $"TICKET: ID:{Id}    DSC: {descripton}   BUG: {bugId}    APP: {appId}    PRIORITY: {priorityId}  STATUS: {statusId}  RES: {resolution}   DTC: {dateCreated}  DTR: {dateResolved}";
        }
    }
}
