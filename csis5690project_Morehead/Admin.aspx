﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="csis5690project_Morehead.Admin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large" Text="Administrator Page"></asp:Label>
    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Size="Medium" Font-Underline="True" Text="Staff Member Section:"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblStaffMessage" runat="server"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </p>
    <p>
        <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Username:"></asp:Label>
&nbsp;<asp:TextBox ID="txtStaffUsername" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Underline="True" Text="Job Code Adding/Updating"></asp:Label>
    </p>
    <p>
        <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Password:"></asp:Label>
&nbsp;<asp:TextBox ID="txtStaffPassword" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label7" runat="server" Text="Code Description:"></asp:Label>
        &nbsp;<asp:TextBox ID="txtCodeDescription" runat="server"></asp:TextBox>
        <asp:Button ID="btnAddCode" runat="server" OnClick="btnAddCode_Click" Text="Add" Width="67px" />
    &nbsp;<asp:Button ID="btnCancelCodeUpdate" runat="server" OnClick="btnCancelCodeUpdate_Click" Text="Cancel" Width="73px" />
    </p>
    <p>
        <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="Job Code:"></asp:Label>
&nbsp;<asp:DropDownList ID="drpJobCodes" runat="server">
        </asp:DropDownList>
&nbsp;<asp:Button ID="btnEditCode" runat="server" OnClick="btnEditCode_Click" Text="Edit" Width="77px" />
&nbsp;<asp:Button ID="btnDeleteCode" runat="server" OnClick="btnDeleteCode_Click" Text="Delete" Width="74px" />
    </p>
    <p>
        <asp:Button ID="btnAddStaff" runat="server" OnClick="btnAddStaff_Click" Text="Add" Width="71px" />
    </p>
        <asp:HiddenField ID="hdnCodeId0" runat="server" />





    <asp:Panel ID="pnlStaffs" runat="server">
        <br />
        <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="Medium" Font-Underline="True" Text="Existing Staff Members"></asp:Label>
       
        
        
        
        <asp:GridView ID="grdStaffs" runat="server"
            AutoGenerateColumns="true"
            HeaderStyle-HorizontalAlign="Center"
            OnRowEditing="grdStaffs_RowEditing"
            OnRowUpdating="grdStaffs_RowUpdating"
            OnRowCancelingEdit="grdStaffs_RowCancelingEdit"
            OnRowDeleting="grdStaffs_RowDeleting"
            OnRowDataBound="grdStaffs_RowDataBound"
            OnRowCreated="grdStaffs_RowCreated"
            >

             <Columns>
                 
                <asp:CommandField ShowEditButton ="true" />
                <asp:CommandField ShowDeleteButton ="true" />

            </Columns>
        </asp:GridView>


    </asp:Panel>








    <p>
        &nbsp;</p>
    <p>
        <asp:HiddenField ID="hdnStaffId" runat="server" />
    </p>
    <p>
        <asp:HiddenField ID="hdnCodeId" runat="server" />
    </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Size="Medium" Font-Underline="True" Text="Ticket Dropdown Population Section:"></asp:Label>
    </p>
    <p>
        <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Size="Small" Font-Underline="True" Text="Statuses"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblStatusMessage" runat="server"></asp:Label>
    </p>



    <asp:Panel ID="pnlStatus" runat="server">
        <asp:GridView ID="grdStatuses" runat="server"
            AutoGenerateColumns="false"
            HeaderStyle-HorizontalAlign="Center"
            OnRowEditing="grdStatuses_RowEditing"
            OnRowUpdating="grdStatuses_RowUpdating"
            OnRowCancelingEdit="grdStatuses_RowCancelingEdit"
            OnRowDeleting="grdStatuses_RowDeleting"
            >
             <Columns>
                <asp:TemplateField HeaderText="ID">
                    <ItemTemplate>
                        <asp:Label ID="Id" runat="server" Text='<%#Eval("Id") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="Status">
                     <ItemTemplate>
                         <asp:Label ID="Status" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                     </ItemTemplate>
                     <EditItemTemplate>
                         <asp:TextBox ID="txtStatusName" runat="server" Text='<%#Eval("Name")%>'></asp:TextBox>
                     </EditItemTemplate>
                 </asp:TemplateField>


                <asp:CommandField ShowEditButton ="true" />
                <asp:CommandField ShowDeleteButton ="true" />
            </Columns>

        </asp:GridView>
    </asp:Panel>



    <p>
        <asp:HiddenField ID="hdnStatusId" runat="server" />
    </p>
    <p>
        <asp:Label ID="Label13" runat="server" Font-Bold="True" Text="Status:"></asp:Label>
&nbsp;<asp:TextBox ID="txtStatus" runat="server"></asp:TextBox>
&nbsp;<asp:Button ID="btnAddStatus" runat="server" OnClick="btnAddStatus_Click" Text="Add" Width="73px" />
    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="Label14" runat="server" Font-Bold="True" Font-Size="Small" Font-Underline="True" Text="Priorities"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblPriorityMessage" runat="server"></asp:Label>
    </p>




    <asp:Panel ID="pnlPriority" runat="server">
        <asp:GridView ID="grdPriorities" runat="server"
            AutoGenerateColumns="false"
            HeaderStyle-HorizontalAlign="Center"
            OnRowEditing="grdPriorities_RowEditing"
            OnRowUpdating="grdPriorities_RowUpdating"
            OnRowCancelingEdit="grdPriorities_RowCancelingEdit"
            OnRowDeleting="grdPriorities_RowDeleting"
            >
             <Columns>
                <asp:TemplateField HeaderText="ID">
                    <ItemTemplate>
                        <asp:Label ID="Id" runat="server" Text='<%#Eval("Id") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="Priority">
                     <ItemTemplate>
                         <asp:Label ID="Priority" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                     </ItemTemplate>
                     <EditItemTemplate>
                         <asp:TextBox ID="txtPriorityName" runat="server" Text='<%#Eval("Name")%>'></asp:TextBox>
                     </EditItemTemplate>
                 </asp:TemplateField>


                <asp:CommandField ShowEditButton ="true" />
                <asp:CommandField ShowDeleteButton ="true" />
            </Columns>


        </asp:GridView>
    </asp:Panel>





    <p>
        <asp:HiddenField ID="hdnPriorityId" runat="server" />
    </p>
    <p>
        <asp:Label ID="Label15" runat="server" Font-Bold="True" Text="Priority:"></asp:Label>
&nbsp;<asp:TextBox ID="txtPriority" runat="server"></asp:TextBox>
&nbsp;<asp:Button ID="btnAddPriority" runat="server" OnClick="btnAddPriority_Click" Text="Add" Width="78px" />
    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="Label16" runat="server" Font-Bold="True" Font-Size="Small" Font-Underline="True" Text="Bugs"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblBugMessage" runat="server"></asp:Label>
    </p>



    <asp:Panel ID="pnlBug" runat="server">
        <asp:GridView ID="grdBugs" runat="server"
            AutoGenerateColumns="false"
            HeaderStyle-HorizontalAlign="Center"
            OnRowEditing="grdBugs_RowEditing"
            OnRowUpdating="grdBugs_RowUpdating"
            OnRowCancelingEdit="grdBugs_RowCancelingEdit"
            OnRowDeleting="grdBugs_RowDeleting"
            >
             <Columns>
                <asp:TemplateField HeaderText="ID">
                    <ItemTemplate>
                        <asp:Label ID="Id" runat="server" Text='<%#Eval("Id") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="Bug">
                     <ItemTemplate>
                         <asp:Label ID="Bug" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                     </ItemTemplate>
                     <EditItemTemplate>
                         <asp:TextBox ID="txtBugName" runat="server" Text='<%#Eval("Name")%>'></asp:TextBox>
                     </EditItemTemplate>
                 </asp:TemplateField>


                <asp:CommandField ShowEditButton ="true" />
                <asp:CommandField ShowDeleteButton ="true" />
            </Columns>


        </asp:GridView>
    </asp:Panel>





    <p>
        <asp:HiddenField ID="hdnBugId" runat="server" />
    </p>
    <p>
        <asp:Label ID="Label17" runat="server" Font-Bold="True" Text="Bug:"></asp:Label>
&nbsp;<asp:TextBox ID="txtBug" runat="server"></asp:TextBox>
&nbsp;<asp:Button ID="btnAddBug" runat="server" OnClick="btnAddBug_Click" Text="Add" Width="78px" />
    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Size="Small" Font-Underline="True" Text="Software Applications"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblAppMessage" runat="server"></asp:Label>
    </p>



    <asp:Panel ID="pnlApp" runat="server">
        <asp:GridView ID="grdApps" runat="server"
            AutoGenerateColumns="false"
            HeaderStyle-HorizontalAlign="Center"
            OnRowEditing="grdApps_RowEditing"
            OnRowUpdating="grdApps_RowUpdating"
            OnRowCancelingEdit="grdApps_RowCancelingEdit"
            OnRowDeleting="grdApps_RowDeleting"
            >
             <Columns>
                <asp:TemplateField HeaderText="ID">
                    <ItemTemplate>
                        <asp:Label ID="Id" runat="server" Text='<%#Eval("Id") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="Software Application">
                     <ItemTemplate>
                         <asp:Label ID="SoftwareApp" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                     </ItemTemplate>
                     <EditItemTemplate>
                         <asp:TextBox ID="txtAppName" runat="server" Text='<%#Eval("Name")%>'></asp:TextBox>
                     </EditItemTemplate>
                 </asp:TemplateField>


                <asp:CommandField ShowEditButton ="true" />
                <asp:CommandField ShowDeleteButton ="true" />
            </Columns>

        </asp:GridView>
    </asp:Panel>



    <p>
        <asp:HiddenField ID="hdnAppId" runat="server" />
    </p>
    <p>
        <asp:Label ID="Label19" runat="server" Font-Bold="True" Text="Sofware Application:"></asp:Label>
&nbsp;<asp:TextBox ID="txtApp" runat="server"></asp:TextBox>
&nbsp;<asp:Button ID="btnAddApp" runat="server" OnClick="btnAddApp_Click" Text="Add" Width="75px" />
    </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Size="Medium" Font-Underline="True" Text="Ticket Search:"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblSearchMessage" runat="server"></asp:Label>
    </p>
    <asp:Panel ID="step1Panel" runat="server">
        <asp:Label ID="Label20" runat="server" Font-Bold="True" Font-Underline="True" Text="Step 1: Choice Filter Option"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Underline="True" Text="OR"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label22" runat="server" Font-Bold="True" Font-Underline="True" Text="Display All Tickets"></asp:Label>
        <br />
        <asp:Label ID="Label23" runat="server" Font-Bold="True" Text="Filter: "></asp:Label>
        <asp:DropDownList ID="drpFilter" runat="server">
            <asp:ListItem Value="Description"></asp:ListItem>
            <asp:ListItem Value="Bug"></asp:ListItem>
            <asp:ListItem Value="Software Application"></asp:ListItem>
            <asp:ListItem Value="Status"></asp:ListItem>
            <asp:ListItem Value="Priority"></asp:ListItem>
        </asp:DropDownList>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnDisplayAll" runat="server" OnClick="btnDisplayAll_Click" Text="Display All" />
        <br />
    </asp:Panel>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Underline="True" Text="Step 2: Fill out Filter based on option"></asp:Label>
    </p>
    <p>
        <asp:Label ID="Label25" runat="server" Font-Bold="True" Text="Description:"></asp:Label>
&nbsp;<asp:TextBox ID="txtDescription" runat="server" Height="26px" TextMode="MultiLine" Width="273px"></asp:TextBox>
    </p>
    <p>
        <asp:Label ID="Label26" runat="server" Font-Bold="True" Text="Bug:"></asp:Label>
&nbsp;<asp:DropDownList ID="drpBugs" runat="server">
        </asp:DropDownList>
    </p>
    <p>
        <asp:Label ID="Label27" runat="server" Font-Bold="True" Text="Sofware Application:"></asp:Label>
&nbsp;<asp:DropDownList ID="drpApps" runat="server">
        </asp:DropDownList>
    </p>
    <p>
        <asp:Label ID="Label28" runat="server" Font-Bold="True" Text="Status:"></asp:Label>
&nbsp;<asp:DropDownList ID="drpStatuses" runat="server">
        </asp:DropDownList>
    </p>
    <p>
        <asp:Label ID="Label29" runat="server" Font-Bold="True" Text="Priority:"></asp:Label>
&nbsp;<asp:DropDownList ID="drpPriorities" runat="server">
        </asp:DropDownList>
    </p>
    <asp:Panel ID="resultsPanel" runat="server">
        <asp:Label ID="Label31" runat="server" Text="Resolution:" Font-Bold="True"></asp:Label>
        &nbsp;<asp:TextBox ID="txtResolution" runat="server" Height="33px" TextMode="MultiLine" Width="286px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label32" runat="server" Font-Bold="True" Text="Date Resolved:"></asp:Label>
        &nbsp;<asp:TextBox ID="txtDateResolved" runat="server" Width="154px"></asp:TextBox>
    </asp:Panel>
    <p>
    </p>
    <p>
        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" />
    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="Label30" runat="server" Font-Bold="True" Font-Size="Small" Text="Results"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblResults" runat="server"></asp:Label>
    </p>


    <asp:Panel ID="pnlResult" runat="server">
        <asp:GridView ID="grdResults" runat="server"
            AutoGenerateColumns="false" 
            OnRowEditing="grdResults_RowEditing" 
            OnRowUpdating="grdResults_RowUpdating" 
            OnRowCancelingEdit="grdResults_RowCancelingEdit" 
            OnRowDeleting="grdResults_RowDeleting"
            >

             <Columns>
              <asp:TemplateField HeaderText="ID">
                  <ItemTemplate>
                      <asp:Label ID="Id" runat="server" Text='<%#Eval("Id") %>'></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:Label ID="Description" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Bug">
                    <ItemTemplate>
                        <asp:Label ID="Bug" runat="server" Text='<%#Eval("Bug.Name") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="SoftwareApp">
                    <ItemTemplate>
                        <asp:Label ID="SoftwareApp" runat="server" Text='<%#Eval("SoftwareApp.Name") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <asp:Label ID="Status" runat="server" Text='<%#Eval("Status.Name") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="Priority">
                    <ItemTemplate>
                        <asp:Label ID="Priority" runat="server" Text='<%#Eval("Priority.Name") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                                 <asp:TemplateField HeaderText="Date Created">
                    <ItemTemplate>
                        <asp:Label ID="DateCreated" runat="server" Text='<%#Eval("DateCreated") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
              
                <asp:TemplateField HeaderText="Resolution">
                    <ItemTemplate>
                        <asp:Label ID="Resolution" runat="server" Text='<%#Eval("Resolution") %>' ></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Date Resolved">
                    <ItemTemplate>
                        <asp:Label ID="DateResolved" runat="server" Text='<%#Eval("DateResolved") %>' ></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>


              <asp:CommandField ShowEditButton="true" />
              <asp:CommandField ShowDeleteButton="true" />
            </Columns>


        </asp:GridView>



        <br />
        <asp:Button ID="btnClearResults" runat="server" OnClick="btnClearResults_Click" Text="Clear Results" />
    </asp:Panel>
    <p>
        &nbsp;</p>
</asp:Content>
