﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using csis5690.Domain;
using csis5690.DAL;
using csis5690.BL;
using System.Data;

namespace csis5690project_Morehead
{
    public partial class Admin : System.Web.UI.Page
    {
        private static readonly ILog logger =
           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private StaffBO staffbo;
        private JobCodeBO codebo;
        private StatusBO statusbo;
        private PriorityBO prioritybo;
        private BugBO bugbo;
        private SoftwareAppBO appbo;
        private TicketBO ticketbo;

        Staff stafftemp;
        JobCode codetemp;
        Status statustemp;
        Priority prioritytemp;
        Bug bugtemp;
        SoftwareApp apptemp;
        Ticket temp;

        protected void Page_Load(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();

            staffbo = new StaffBO("localhost");
            codebo = new JobCodeBO("localhost");
            statusbo = new StatusBO("localhost");
            prioritybo = new PriorityBO("localhost");
            bugbo = new BugBO("localhost");
            appbo = new SoftwareAppBO("localhost");
            ticketbo = new TicketBO("localhost");

            if (Page.IsPostBack)
            {
                //not the first time in page
            }
            else
            {
                //first time in page
                PopulateJobCodeDropDown();
                PopulateStaffGridView();

                PopulateStatusGridView();
                PopulatePriorityGridView();
                PopulateBugGridView();
                PopulateAppGridView();

                btnCancelCodeUpdate.Visible = false;
                step1Panel.Visible = true;
                pnlResult.Visible = false;
                PopulateDataControls();
                lblSearchMessage.Text = "Choose 1 filter to use in your search [Description, Bug, Software Application, Status or Priority] OR Choose to display all tickets.";
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;
            }
        }

        //Staff Items
        private void PopulateJobCodeDropDown()
        {
            try
            {
                drpJobCodes.DataSource = codebo.SelectManyObjects(new JobCode(-1, "%"));
                drpJobCodes.DataValueField = "Id";
                drpJobCodes.DataTextField = "Description";
                drpJobCodes.DataBind();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        private void PopulateStaffGridView()
        {
            try
            {
                DataTable staffs = staffbo.SelectManyWithJobCode();
                //IList<object> codes = codebo.SelectManyObjects(new JobCode(-1, "%"));

                //if ((staffs.DataSet.Tables.Count > 0) && (staffs.Rows.Count > 0))
                if (staffs.Rows.Count > 0)
                {
                    pnlStaffs.Visible = true;
                    grdStaffs.DataSource = staffs;
                    grdStaffs.DataBind();
                    hdnStaffId.Value = string.Empty;
                }
                else
                {
                    lblStaffMessage.Text = "No Staff Members currently exist. Please add a Staff Member.";
                    lblStaffMessage.ForeColor = System.Drawing.Color.OrangeRed;
                    pnlStaffs.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        protected void btnAddStaff_Click(object sender, EventArgs e)
        {
            lblStaffMessage.Text = string.Empty;
            string staffUsername = txtStaffUsername.Text;
            string staffPassword = txtStaffPassword.Text;
            int jobCode = Convert.ToInt32(drpJobCodes.SelectedValue);

            try
            {
                //doing an insert or add
                stafftemp = new Staff(-1, staffUsername, staffPassword, jobCode);
                staffbo.InsertOneObject(stafftemp);
                lblStaffMessage.Text = "Staff Member Added Successfully!!!";
                lblStaffMessage.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                lblStaffMessage.Text = "Staff Addition Error!!! Please try again.";
                lblStaffMessage.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {
                PopulateStaffGridView();
                hdnStaffId.Value = string.Empty;
                txtStaffUsername.Text = string.Empty;
                txtStaffPassword.Text = string.Empty;

                //clear out all other message labels
                ClearLabels(lblStaffMessage);
            }
        }

        protected void grdStaffs_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdStaffs.EditIndex = e.NewEditIndex;
            PopulateStaffGridView();

            //hide Add Staffs and Code Items
            btnAddStaff.Visible = false;
            btnAddCode.Visible = false;
            btnEditCode.Visible = false;
            btnDeleteCode.Visible = false;
            

            GridViewRow row = grdStaffs.Rows[e.NewEditIndex];
            row.ForeColor = System.Drawing.Color.Red; //turn row red for user friendly look
            TextBox txtUsername = (TextBox)row.Cells[1].Controls[0];
            txtStaffUsername.Text = txtUsername.Text; //populate username textbox with what is in the gridview row

            TextBox txtPassword = (TextBox)row.Cells[2].Controls[0];
            txtStaffPassword.Text = txtPassword.Text; //populate password textbox with what is in the gridview row

            //tell user what staff member they are updating
            TextBox txtId = (TextBox)row.Cells[0].Controls[0];
            //logger.Debug($"Staff ID: {txtId.Text}");
            lblStaffMessage.Text = $"You are about to update staff member #{txtId.Text}!!!";
            lblStaffMessage.ForeColor = System.Drawing.Color.Red; //make message red

            //clear out all other message labels
            ClearLabels(lblStaffMessage);

        }

        protected void grdStaffs_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                GridViewRow row = grdStaffs.Rows[e.RowIndex];
                TextBox txtStaffId = (TextBox)row.Cells[0].Controls[0]; //readonly
                hdnStaffId.Value = txtStaffId.Text;
                int staffId = Convert.ToInt32(hdnStaffId.Value);

                /*
                GridViewRow row = grdStaffs.Rows[e.RowIndex];
                TextBox txtStaffId = (TextBox)row.Cells[0].Controls[0]; //readonly
                TextBox txtUsername = (TextBox)row.Cells[1].Controls[0];
                txtStaffUsername.Text = txtUsername.Text; //populate username textbox with what is in the gridview row

                TextBox txtPassword = (TextBox)row.Cells[2].Controls[0];
                txtStaffPassword.Text = txtPassword.Text; //populate password textbox with what is in the gridview row

                //DropDownList codeDrp = (row.Cells[3].FindControl("codeDrp") as DropDownList);

                hdnStaffId.Value = txtStaffId.Text;
                logger.Debug($"Staff ID Hidden Value: {hdnStaffId.Value}");
                int staffId = Convert.ToInt32(hdnStaffId.Value);

                //hdnCodeId.Value = codeDrp.SelectedValue;
                //logger.Debug($"Code Hidden Value: {hdnCodeId.Value}");
                //int codeId = Convert.ToInt32(drpJobCodes.SelectedValue);

                logger.Debug($"Staff ID [hidden field]: {staffId.ToString()}");
                logger.Debug($"Username [changed/new]: {txtUsername.Text}");
                logger.Debug($"Password [changed/new]: {txtPassword.Text}");
                //logger.Debug($"Selected Job Code [From Dropdown in Gridview]: {codeId.ToString()}");
              */

                logger.Debug($"ID: {staffId}");
                logger.Debug($"Username: {txtStaffUsername.Text}");
                logger.Debug($"Password: {txtStaffPassword.Text}");
                logger.Debug($"Job Code ID: {drpJobCodes.SelectedValue}");


                Staff temp = new Staff(staffId, txtStaffUsername.Text, txtStaffPassword.Text, Convert.ToInt32(drpJobCodes.SelectedValue));

                staffbo.UpdateOneObject(temp);
                grdStaffs.EditIndex = -1;

                //display message to user -- successful update
                lblStaffMessage.Text = $"You have successffuly updated staff member #{staffId}";
                lblStaffMessage.ForeColor = System.Drawing.Color.Green; //make message green
            }
            catch (Exception ex)
            {
                //display message to user -- error update
                lblStaffMessage.Text = $"Updae Failed. Please try again.";
                lblStaffMessage.ForeColor = System.Drawing.Color.Red; //make message green

                logger.Error(ex.Message);
            }
            finally
            {
                PopulateStaffGridView();
            }
           
            //clear textboxes after update
            txtStaffUsername.Text = string.Empty;
            txtStaffPassword.Text = string.Empty;
            //make add staffs button visible again
            btnAddStaff.Visible = true;
            btnAddCode.Visible = true;
            btnEditCode.Visible = true;
            btnDeleteCode.Visible = true;

            //clear out all other message labels
            ClearLabels(lblStaffMessage);

        }

        protected void grdStaffs_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdStaffs.EditIndex = -1;
            lblStaffMessage.Text = "Staff Member Edit Cancelled";
            lblStaffMessage.ForeColor = System.Drawing.Color.Red;
            PopulateStaffGridView();

            //clear out all other message labels
            ClearLabels(lblStaffMessage);
        }

        protected void grdStaffs_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GridViewRow row = grdStaffs.Rows[e.RowIndex];

                logger.Debug($"Cell Index [delete]: {row.Cells[0].Text}");

                int txtStaffId = Convert.ToInt32(row.Cells[0].Text);

                //hdnStaffId.Value = txtStaffId.ToString();
                //int staffId = Convert.ToInt32(hdnStaffId.Value);

                //logger.Debug($"Staff ID [hidden field]: {staffId.ToString()}");

                stafftemp = new Staff(txtStaffId, "%", "%", -1);

                staffbo.DeleteOneObject(stafftemp);

                lblStaffMessage.Text = "Staff Member Deleted Successfully!!!";
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            finally
            {
                PopulateStaffGridView();
                //clear out all other message labels
                ClearLabels(lblStaffMessage);
            }
            
        }

        protected void grdStaffs_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                /*
                TextBox txtValue = (e.Row.Cells[3].Controls[0] as TextBox);

                DropDownList codeDrp = (e.Row.Cells[3].FindControl("codeDrp") as DropDownList);
                codeDrp.DataSource = codebo.SelectManyObjects(new JobCode(-1, "%"));
                codeDrp.DataValueField = "Id";
                codeDrp.DataTextField = "Description";
                codeDrp.DataBind();

                codeDrp.SelectedValue = txtValue.Text;
                e.Row.Cells[3].Controls.Add(codeDrp);
                txtValue.Visible = false;
                */

                ((TextBox)e.Row.Cells[0].Controls[0]).Enabled = false; //make ID column readonly
                ((TextBox)e.Row.Cells[1].Controls[0]).Enabled = false; //make Username column readonly
                ((TextBox)e.Row.Cells[2].Controls[0]).Enabled = false; //make Password column readonly
                ((TextBox)e.Row.Cells[3].Controls[0]).Enabled = false; //make Job Code ID column readonly
                ((TextBox)e.Row.Cells[4].Controls[0]).Enabled = false; //make Job Code Description column readonly

            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        //moves autogenerated columns to far left side - gridview buttons to far right side
        protected void grdStaffs_RowCreated(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;

            List<TableCell> columns = new List<TableCell>();

            foreach (DataControlField column in grdStaffs.Columns)
            {
                TableCell cell = row.Cells[0];

                row.Cells.Remove(cell);

                columns.Add(cell);
            }

            row.Cells.AddRange(columns.ToArray());
        }


        //Job Code Items
        protected void btnAddCode_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnAddCode.Text == "Update")
                {
                    //doing an update
                    logger.Debug($"Code ID: {hdnCodeId.Value}");
                    int codeId = Convert.ToInt32(hdnCodeId.Value);
                    string codeDescription = txtCodeDescription.Text;

                    JobCode filter = new JobCode(codeId, codeDescription);
                    codebo.UpdateOneObject(filter);

                    lblStaffMessage.Text = "Job Code Edited Successfully!!!";
                    lblStaffMessage.ForeColor = System.Drawing.Color.Green;

                    txtCodeDescription.Text = string.Empty;
                    btnEditCode.Visible = true;
                    btnDeleteCode.Visible = true;
                    btnCancelCodeUpdate.Visible = false;
                    btnAddStaff.Visible = true;
                    btnAddCode.Text = "Add";
                }
                else
                {
                    lblStaffMessage.Text = string.Empty;
                    string codeDescription = txtCodeDescription.Text;

                    //doing an insert or add
                    codetemp = new JobCode(-1, codeDescription);
                    codebo.InsertOneObject(codetemp);
                    lblStaffMessage.Text = "Job Code Added Successfully";

                    txtCodeDescription.Text = string.Empty;
                    hdnCodeId.Value = string.Empty;
                    lblStaffMessage.ForeColor = System.Drawing.Color.Green;
                    logger.Debug($"Job Code: {codetemp.ToString()}");
                }
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                //lblMessage.ForeColor = System.Drawing.Color.Red;
                logger.Error(ex);
            }
            finally
            {
                PopulateJobCodeDropDown();
                PopulateStaffGridView();

                //clear out all other message labels
                ClearLabels(lblStaffMessage);
            }
        }

        protected void btnEditCode_Click(object sender, EventArgs e)
        {
            lblStaffMessage.Text = string.Empty;
            txtCodeDescription.Text = Convert.ToString(drpJobCodes.SelectedItem);
            hdnCodeId.Value = drpJobCodes.SelectedValue;
            logger.Debug($"Code ID: {hdnCodeId.Value}");

            btnAddCode.Text = "Update";
            btnDeleteCode.Visible = false;
            btnEditCode.Visible = false;
            btnCancelCodeUpdate.Visible = true;
            btnAddStaff.Visible = false;

            PopulateJobCodeDropDown();

            //clear out all other message labels
            ClearLabels(lblStaffMessage);
        }

        protected void btnDeleteCode_Click(object sender, EventArgs e)
        {
            lblStaffMessage.Text = string.Empty;

            try
            {
                IList<object> staffs = staffbo.SelectManyObjects(new Staff(-1, "%", "%", -1));


                int codeId = Convert.ToInt32(drpJobCodes.SelectedValue);
                string codeName = drpJobCodes.SelectedItem.ToString();

                JobCode filter = new JobCode(codeId, "%");
                codetemp = (JobCode)codebo.DeleteOneObject(filter);

                lblStaffMessage.Text = $"Job Code Deleted Successfully!!! Code Deleted: {codeId} - {codeName}";
                lblStaffMessage.ForeColor = System.Drawing.Color.Green;

                txtCodeDescription.Text = string.Empty;
                btnEditCode.Visible = true;

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                lblStaffMessage.Text = "Job code currently in use. Cannot delete code. To delete code update all staff members using code then perform deletion.";
                lblStaffMessage.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {
                PopulateJobCodeDropDown();
                //clear out all other message labels
                ClearLabels(lblStaffMessage);
            }
        }

        protected void btnCancelCodeUpdate_Click(object sender, EventArgs e)
        {
            txtCodeDescription.Text = string.Empty;
            lblStaffMessage.Text = "Job Code Update Cancelled.";
            lblStaffMessage.ForeColor = System.Drawing.Color.Red;
            btnEditCode.Visible = true;
            btnDeleteCode.Visible = true;
            btnCancelCodeUpdate.Visible = false;
            btnAddStaff.Visible = true;
            PopulateJobCodeDropDown();
            //clear out all other message labels
            ClearLabels(lblStaffMessage);
        }


        //Status Items
        private void PopulateStatusGridView()
        {
            try
            {
                IList<object> statuses = statusbo.SelectManyObjects(new Status(-1, "%", DateTime.Now));

                if (statuses.Count > 0)
                {
                    pnlStatus.Visible = true;
                    grdStatuses.DataSource = statuses;
                    grdStatuses.DataBind();
                }
                else
                {
                    lblStatusMessage.Text = "No Statuses currently exist. Please add a Status.";
                    lblStatusMessage.ForeColor = System.Drawing.Color.OrangeRed;
                    pnlStatus.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        protected void btnAddStatus_Click(object sender, EventArgs e)
        {
            lblStatusMessage.Text = string.Empty;
            string statusName = txtStatus.Text;

            try
            {
                //doing an insert or add
                statustemp = new Status(-1, statusName, DateTime.Now);
                statusbo.InsertOneObject(statustemp);
                lblStatusMessage.Text = "Status Added Successfully!!!";
                lblStatusMessage.ForeColor = System.Drawing.Color.Green;
                
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                lblStatusMessage.Text = "Status Addition Error!!! Please try again. ";
                lblStatusMessage.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {
                PopulateStatusGridView();
                hdnStatusId.Value = string.Empty;
                txtStatus.Text = string.Empty;
                //clear out all other message labels
                ClearLabels(lblStatusMessage);
            }
        }

        protected void grdStatuses_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdStatuses.EditIndex = e.NewEditIndex;

            GridViewRow row = grdStatuses.Rows[e.NewEditIndex];
            row.ForeColor = System.Drawing.Color.Red; //turn row red

            lblStatusMessage.Text = "You are about to update a status";
            lblStatusMessage.ForeColor = System.Drawing.Color.Red;

            PopulateStatusGridView();

            btnAddStatus.Visible = false;

            //clear out all other message labels
            ClearLabels(lblStatusMessage);
        }

        protected void grdStatuses_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                GridViewRow row = grdStatuses.Rows[e.RowIndex];

                Label lblStatusId = (Label)row.FindControl("Id");
                TextBox txtStatusName = (TextBox)row.Cells[1].FindControl("txtStatusName");

                int statusId = Convert.ToInt32(lblStatusId.Text);

                Status temp = new Status(statusId, txtStatusName.Text, DateTime.Now);

                statusbo.UpdateOneObject(temp);
                grdStatuses.EditIndex = -1;

                lblStatusMessage.Text = "Status Updated Sucessfully!!!";
                lblStatusMessage.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                lblStatusMessage.Text = "Status Update Error!!! Please try again.";
                lblStatusMessage.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {
                PopulateStatusGridView();
                PopulateStatusDropdown();
                btnAddStatus.Visible = true;
                //clear out all other message labels
                ClearLabels(lblStatusMessage);
            }

        }

        protected void grdStatuses_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdStatuses.EditIndex = -1;
            btnAddStatus.Visible = true;
            lblStatusMessage.Text = "Status Update Cancelled";
            lblStatusMessage.ForeColor = System.Drawing.Color.Red;
            PopulateStatusGridView();
            //clear out all other message labels
            ClearLabels(lblStatusMessage);
        }

        protected void grdStatuses_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GridViewRow row = grdStatuses.Rows[e.RowIndex];
                Label lbStatuslId = (Label)row.FindControl("Id");

                int id = Convert.ToInt32(lbStatuslId.Text);

                statustemp = new Status(id, "%", DateTime.Now);
                statusbo.DeleteOneObject(statustemp);

                lblStatusMessage.Text = "Status Deleted Sucessfully!!!";
                lblStatusMessage.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                lblStatusMessage.Text = "Status currently in use. Please update all tickets with this status to delete this status.";
                lblStatusMessage.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {
                PopulateStatusGridView();

                //clear out all other message labels
                ClearLabels(lblStatusMessage);
            }
            
        }


        //Priority Items
        private void PopulatePriorityGridView()
        {
            try
            {
                IList<object> priorities = prioritybo.SelectManyObjects(new Priority(-1, "%", DateTime.Now));

                if (priorities.Count > 0)
                {
                    pnlPriority.Visible = true;
                    grdPriorities.DataSource = priorities;
                    grdPriorities.DataBind();
                }
                else
                {
                    lblPriorityMessage.Text = "No Priorities currently exist. Please add a Priority.";
                    lblPriorityMessage.ForeColor = System.Drawing.Color.OrangeRed;
                    pnlPriority.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        protected void btnAddPriority_Click(object sender, EventArgs e)
        {
            lblPriorityMessage.Text = string.Empty;
            string priorityName = txtPriority.Text;

            try
            {
                //doing an insert or add
                prioritytemp = new Priority(-1, priorityName, DateTime.Now);
                prioritybo.InsertOneObject(prioritytemp);
                lblPriorityMessage.Text = "Priority Added Successfully!!!";
                lblPriorityMessage.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                lblPriorityMessage.Text = "Priority Addition Error!!! Please try again. ";
                lblPriorityMessage.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {
                PopulatePriorityGridView();
                hdnPriorityId.Value = string.Empty;
                txtPriority.Text = string.Empty;
                //clear out all other message labels
                ClearLabels(lblPriorityMessage);
            }
        }

        protected void grdPriorities_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdPriorities.EditIndex = e.NewEditIndex;

            GridViewRow row = grdPriorities.Rows[e.NewEditIndex];
            row.ForeColor = System.Drawing.Color.Red; //turn row red

            lblPriorityMessage.Text = "You are about to update a priority";
            lblPriorityMessage.ForeColor = System.Drawing.Color.Red;

            PopulatePriorityGridView();

            btnAddPriority.Visible = false;
            //clear out all other message labels
            ClearLabels(lblPriorityMessage);
        }

        protected void grdPriorities_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                GridViewRow row = grdPriorities.Rows[e.RowIndex];

                Label lblPriorityId = (Label)row.FindControl("Id");
                TextBox txtPriorityName = (TextBox)row.Cells[1].FindControl("txtPriorityName");

                int priorityId = Convert.ToInt32(lblPriorityId.Text);

                Priority temp = new Priority(priorityId, txtPriorityName.Text, DateTime.Now);

                prioritybo.UpdateOneObject(temp);
                grdPriorities.EditIndex = -1;

                lblPriorityMessage.Text = "Priority Updated Sucessfully!!!";
                lblPriorityMessage.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                lblPriorityMessage.Text = "Priority Update Error!!! Please try again.";
                lblPriorityMessage.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {
                PopulatePriorityGridView();
                PopulatePriorityDropdown();
                btnAddPriority.Visible = true;
                //clear out all other message labels
                ClearLabels(lblPriorityMessage);
            }
            
        }

        protected void grdPriorities_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            btnAddPriority.Visible = true;
            grdPriorities.EditIndex = -1;
            lblPriorityMessage.Text = $"Priority Update Cancelled";
            lblPriorityMessage.ForeColor = System.Drawing.Color.Red;
            PopulatePriorityGridView();
            //clear out all other message labels
            ClearLabels(lblPriorityMessage);
        }

        protected void grdPriorities_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GridViewRow row = grdPriorities.Rows[e.RowIndex];
                Label lbPrioritylId = (Label)row.FindControl("Id");

                int id = Convert.ToInt32(lbPrioritylId.Text);

                prioritytemp = new Priority(id, "%", DateTime.Now);
                prioritybo.DeleteOneObject(prioritytemp);

                lblPriorityMessage.Text = "Priority Deleted Sucessfully!!!";
                lblPriorityMessage.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                lblPriorityMessage.Text = "Priority currently in use. Please update all tickets with this priority to delete this priority.";
                lblPriorityMessage.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {
                PopulatePriorityGridView();

                //clear out all other message labels
                ClearLabels(lblPriorityMessage);
            }

        }

        //Bug Items
        private void PopulateBugGridView()
        {
            try
            {
                IList<object> bugs = bugbo.SelectManyObjects(new Bug(-1, "%", DateTime.Now));

                if (bugs.Count > 0)
                {
                    pnlBug.Visible = true;
                    grdBugs.DataSource = bugs;
                    grdBugs.DataBind();
                }
                else
                {
                    lblBugMessage.Text = "No Bugs currently exist. Please add a Bug.";
                    lblBugMessage.ForeColor = System.Drawing.Color.OrangeRed;
                    pnlBug.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        protected void btnAddBug_Click(object sender, EventArgs e)
        {
            lblBugMessage.Text = string.Empty;
            string bugName = txtBug.Text;

            try
            {
                //doing an insert or add
                bugtemp = new Bug(-1, bugName, DateTime.Now);
                bugbo.InsertOneObject(bugtemp);
                lblBugMessage.Text = "Bug Added Successfully!!!";
                lblBugMessage.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                lblBugMessage.Text = "Bug Addition Error!!! Please try again. ";
                lblBugMessage.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {
                PopulateBugGridView();
                hdnBugId.Value = string.Empty;
                txtBug.Text = string.Empty;
                //clear out all other message labels
                ClearLabels(lblBugMessage);
            }
        }

        protected void grdBugs_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdBugs.EditIndex = e.NewEditIndex;

            GridViewRow row = grdBugs.Rows[e.NewEditIndex];
            row.ForeColor = System.Drawing.Color.Red; //turn row red

            lblBugMessage.Text = "You are about to update a bug";
            lblBugMessage.ForeColor = System.Drawing.Color.Red;

            PopulateBugGridView();

            btnAddBug.Visible = false;
            //clear out all other message labels
            ClearLabels(lblBugMessage);
        }

        protected void grdBugs_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                GridViewRow row = grdBugs.Rows[e.RowIndex];

                Label lblBugId = (Label)row.FindControl("Id");
                TextBox txtBugName = (TextBox)row.Cells[1].FindControl("txtBugName");

                int bugId = Convert.ToInt32(lblBugId.Text);

                Bug temp = new Bug(bugId, txtBugName.Text, DateTime.Now);

                bugbo.UpdateOneObject(temp);
                grdBugs.EditIndex = -1;

                lblBugMessage.Text = "Bug Updated Sucessfully!!!";
                lblBugMessage.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                lblBugMessage.Text = "Bug Update Error!!! Please try again.";
                lblBugMessage.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {
                PopulateBugGridView();
                PopulateBugDropdown();
                btnAddBug.Visible = true;
                //clear out all other message labels
                ClearLabels(lblBugMessage);
            }
            
        }

        protected void grdBugs_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdBugs.EditIndex = -1;
            btnAddBug.Visible = true;
            lblBugMessage.Text = "Bug Update Cancelled";
            lblBugMessage.ForeColor = System.Drawing.Color.Red;
            PopulateBugGridView();
            //clear out all other message labels
            ClearLabels(lblBugMessage);
        }

        protected void grdBugs_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GridViewRow row = grdBugs.Rows[e.RowIndex];
                Label lblBugId = (Label)row.FindControl("Id");

                int id = Convert.ToInt32(lblBugId.Text);

                bugtemp = new Bug(id, "%", DateTime.Now);
                bugbo.DeleteOneObject(bugtemp);

                lblBugMessage.Text = "Bug Deleted Sucessfully!!!";
                lblBugMessage.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                lblBugMessage.Text = "Bug currently in use. Please update all tickets with this bug to delete this bug.";
                lblBugMessage.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {
                PopulateBugGridView();

                //clear out all other message labels
                ClearLabels(lblBugMessage);
            }
           
        }

        //Software Application Items
        private void PopulateAppGridView()
        {
            try
            {
                IList<object> apps = appbo.SelectManyObjects(new SoftwareApp(-1, "%", DateTime.Now));

                if (apps.Count > 0)
                {
                    pnlApp.Visible = true;
                    grdApps.DataSource = apps;
                    grdApps.DataBind();
                }
                else
                {
                    lblAppMessage.Text = "No Sofware Applicationss currently exist. Please add a Software Application.";
                    lblAppMessage.ForeColor = System.Drawing.Color.OrangeRed;
                    pnlApp.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        protected void btnAddApp_Click(object sender, EventArgs e)
        {
            lblAppMessage.Text = string.Empty;
            string appName = txtApp.Text;

            try
            {
                //doing an insert or add
                apptemp = new SoftwareApp(-1, appName, DateTime.Now);
                appbo.InsertOneObject(apptemp);
                lblAppMessage.Text = "Software Application Added Successfully!!!";
                lblAppMessage.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                lblAppMessage.Text = "Software Application Addition Error!!! Please try again. ";
                lblAppMessage.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {
                PopulateAppGridView();
                hdnAppId.Value = string.Empty;
                txtApp.Text = string.Empty;
                //clear out all other message labels
                ClearLabels(lblAppMessage);
            }
        }

        protected void grdApps_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdApps.EditIndex = e.NewEditIndex;

            GridViewRow row = grdApps.Rows[e.NewEditIndex];
            row.ForeColor = System.Drawing.Color.Red;

            lblAppMessage.Text = "You are about to update a software application";
            lblAppMessage.ForeColor = System.Drawing.Color.Red;

            PopulateAppGridView();

            btnAddApp.Visible = false;
            //clear out all other message labels
            ClearLabels(lblAppMessage);
        }

        protected void grdApps_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                GridViewRow row = grdApps.Rows[e.RowIndex];

                Label lblAppId = (Label)row.FindControl("Id");
                TextBox txtAppName = (TextBox)row.Cells[1].FindControl("txtAppName");

                int appId = Convert.ToInt32(lblAppId.Text);

                SoftwareApp temp = new SoftwareApp(appId, txtAppName.Text, DateTime.Now);

                appbo.UpdateOneObject(temp);
                grdApps.EditIndex = -1;

                lblAppMessage.Text = "Software Applictaion Updated Sucessfully!!!";
                lblAppMessage.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                lblAppMessage.Text = "Software Application Update Error!!! Please try again.";
                lblAppMessage.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {
                PopulateAppGridView();
                PopulateAppDropdown();
                btnAddApp.Visible = true;
                //clear out all other message labels
                ClearLabels(lblAppMessage);
            }

        }

        protected void grdApps_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdApps.EditIndex = -1;
            btnAddApp.Visible = true;
            lblAppMessage.Text = "Software Application Update Cancelled";
            lblAppMessage.ForeColor = System.Drawing.Color.Red;
            PopulateAppGridView();
            //clear out all other message labels
            ClearLabels(lblAppMessage);
        }

        protected void grdApps_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GridViewRow row = grdApps.Rows[e.RowIndex];
                Label lblAppId = (Label)row.FindControl("Id");

                int id = Convert.ToInt32(lblAppId.Text);

                apptemp = new SoftwareApp(id, "%", DateTime.Now);
                appbo.DeleteOneObject(apptemp);

                lblAppMessage.Text = "Software Application Deleted Sucessfully!!!";
                lblAppMessage.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                lblAppMessage.Text = "Software Application currently in use. Please update all tickets with this software application to delete this software application.";
                lblAppMessage.ForeColor = System.Drawing.Color.Red;

                //clear all other message labels
                lblStaffMessage.Text = string.Empty;
                lblStatusMessage.Text = string.Empty;
                lblPriorityMessage.Text = string.Empty;
                lblBugMessage.Text = string.Empty;
            }
            finally
            {
                PopulateAppGridView();

                //clear out all other message labels
                ClearLabels(lblAppMessage);
            }
            
        }


        //Ticket Search Items
        private void PopulateDataControls()
        {
            PopulateBugDropdown();
            PopulateAppDropdown();
            PopulateStatusDropdown();
            PopulatePriorityDropdown();
        }

        private void PopulateBugDropdown()
        {
            try
            {
                drpBugs.DataSource = bugbo.SelectManyObjects(new Bug(-1, "%", DateTime.Now));
                drpBugs.DataValueField = "Id";
                drpBugs.DataTextField = "Name";
                drpBugs.DataBind();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        private void PopulateAppDropdown()
        {
            try
            {
                drpApps.DataSource = appbo.SelectManyObjects(new SoftwareApp(-1, "%", DateTime.Now));
                drpApps.DataValueField = "Id";
                drpApps.DataTextField = "Name";
                drpApps.DataBind();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        private void PopulateStatusDropdown()
        {
            try
            {
                drpStatuses.DataSource = statusbo.SelectManyObjects(new Status(-1, "%", DateTime.Now));
                drpStatuses.DataValueField = "Id";
                drpStatuses.DataTextField = "Name";
                drpStatuses.DataBind();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        private void PopulatePriorityDropdown()
        {
            try
            {
                drpPriorities.DataSource = prioritybo.SelectManyObjects(new Priority(-1, "%", DateTime.Now));
                drpPriorities.DataValueField = "Id";
                drpPriorities.DataTextField = "Name";
                drpPriorities.DataBind();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        private void PopulateResultsGrid()
        {
            try
            {
                IList<object> results = ticketbo.SelectManyObjects(new Ticket(-1, "%", -1, -1, -1, -1, "%", DateTime.Now, "%"));

                if (results.Count > 0)
                {
                    grdResults.DataSource = results;
                    grdResults.DataBind();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        //Filtering & gridview items
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            lblResults.Text = string.Empty;

            if (drpFilter.Text == "Description")
            {
                try //if description textbox has a value
                {

                    logger.Debug($"Description Search: {txtDescription.Text}");

                    lblSearchMessage.Text = "Searched using 'Description' Filter";
                    lblSearchMessage.ForeColor = System.Drawing.Color.Green;

                    Ticket filter = new Ticket(-1, txtDescription.Text.Trim(), -1, -1, -1, -1, "%", DateTime.Now, "%");

                    IList<object> lstResults = ticketbo.SelectManyByDescription(filter);

                    if (lstResults.Count <= 0)
                    {
                        lblResults.Text = "No Results Found";
                        lblResults.ForeColor = System.Drawing.Color.Red;
                        grdResults.DataSource = null;
                        grdResults.DataBind();
                    }
                    else
                    {
                        lblResults.Text = string.Empty;
                        grdResults.DataSource = lstResults;
                        grdResults.DataBind();
                        pnlResult.Visible = true;
                    }
                }
                catch (Exception ex) //if description textbox is blank catch here
                {
                    logger.Error(ex);
                    lblSearchMessage.Text = "Description cannot be blank.";
                    lblSearchMessage.ForeColor = System.Drawing.Color.Red;
                }

            }
            if (drpFilter.Text == "Bug")
            {
                txtDescription.Text = string.Empty;


                logger.Debug($"ID: {drpBugs.SelectedValue} NAME: {drpBugs.SelectedItem}");

                lblSearchMessage.Text = "Searched using 'Bug' Filter";
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;

                Ticket filter = new Ticket(-1, "%", Convert.ToInt32(drpBugs.SelectedValue), -1, -1, -1, "%", DateTime.Now, "%");

                IList<object> lstResults = ticketbo.SelectManyByBug(filter);

                if (lstResults.Count <= 0)
                {
                    lblResults.Text = "No Results Found";
                    lblResults.ForeColor = System.Drawing.Color.Red;
                    grdResults.DataSource = null;
                    grdResults.DataBind();
                }
                else
                {
                    lblResults.Text = string.Empty;
                    grdResults.DataSource = lstResults;
                    grdResults.DataBind();
                    pnlResult.Visible = true;
                }
            }
            if (drpFilter.Text == "Software Application")
            {
                txtDescription.Text = string.Empty;


                logger.Debug($"ID: {drpApps.SelectedValue} NAME: {drpApps.SelectedItem}");

                lblSearchMessage.Text = "Searched using 'Software Application' Filter";
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;

                Ticket filter = new Ticket(-1, "%", -1, Convert.ToInt32(drpApps.SelectedValue), -1, -1, "%", DateTime.Now, "%");

                IList<object> lstResults = ticketbo.SelectManyBySoftwareApp(filter);

                if (lstResults.Count <= 0)
                {
                    lblResults.Text = "No Results Found";
                    lblResults.ForeColor = System.Drawing.Color.Red;
                    grdResults.DataSource = null;
                    grdResults.DataBind();
                }
                else
                {
                    lblResults.Text = string.Empty;
                    grdResults.DataSource = lstResults;
                    grdResults.DataBind();
                    pnlResult.Visible = true;
                }
            }
            if (drpFilter.Text == "Status")
            {
                txtDescription.Text = string.Empty;

                logger.Debug($"ID: {drpStatuses.SelectedValue} NAME: {drpStatuses.SelectedItem}");

                lblSearchMessage.Text = "Searched using 'Status' Filter";
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;

                Ticket filter = new Ticket(-1, "%", -1, -1, -1, Convert.ToInt32(drpStatuses.SelectedValue), "%", DateTime.Now, "%");

                IList<object> lstResults = ticketbo.SelectManyByStatus(filter);

                if (lstResults.Count <= 0)
                {
                    lblResults.Text = "No Results Found";
                    lblResults.ForeColor = System.Drawing.Color.Red;
                    grdResults.DataSource = null;
                    grdResults.DataBind();
                }
                else
                {
                    lblResults.Text = string.Empty;
                    grdResults.DataSource = lstResults;
                    grdResults.DataBind();
                    pnlResult.Visible = true;
                }
            }
            if (drpFilter.Text == "Priority")
            {
                txtDescription.Text = string.Empty;

                logger.Debug($"ID: {drpPriorities.SelectedValue} NAME: {drpPriorities.SelectedItem}");

                lblSearchMessage.Text = "Searched using 'Priority' Filter";
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;

                Ticket filter = new Ticket(-1, "%", -1, -1, Convert.ToInt32(drpPriorities.SelectedValue), -1, "%", DateTime.Now, "%");

                IList<object> lstResults = ticketbo.SelectManyByPriority(filter);

                if (lstResults.Count <= 0)
                {
                    lblResults.Text = "No Results Found";
                    lblResults.ForeColor = System.Drawing.Color.Red;
                    grdResults.DataSource = null;
                    grdResults.DataBind();
                }
                else
                {
                    lblResults.Text = string.Empty;
                    grdResults.DataSource = lstResults;
                    grdResults.DataBind();
                    pnlResult.Visible = true;
                }
            }

            ClearLabels(lblSearchMessage);

        }

        protected void btnDisplayAll_Click(object sender, EventArgs e)
        {
            Ticket filter = new Ticket(-1, "%", -1, -1, -1, -1, "%", DateTime.Now, "%");

            IList<object> lstResults = ticketbo.SelectManyObjects(filter);

            if (lstResults.Count <= 0)
            {
                lblResults.Text = "No Results Found";
                lblResults.ForeColor = System.Drawing.Color.Red;
                grdResults.DataSource = null;
                grdResults.DataBind();
            }
            else
            {
                lblResults.Text = string.Empty;
                grdResults.DataSource = lstResults;
                grdResults.DataBind();
                pnlResult.Visible = true;
            }

            lblSearchMessage.Text = "Searched using 'Display All' Filter";
            lblSearchMessage.ForeColor = System.Drawing.Color.Green;

            ClearLabels(lblSearchMessage);
        }

        protected void btnClearResults_Click(object sender, EventArgs e)
        {
            ClearLabels(lblResults);
            lblResults.Text = "Results Cleared";
            lblResults.ForeColor = System.Drawing.Color.Green;
            grdResults.DataSource = null;
            grdResults.DataBind();
            pnlResult.Visible = false;
        }

        protected void grdResults_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                logger.Debug($"Edit Index: {e.NewEditIndex}");
                grdResults.EditIndex = e.NewEditIndex;
                PopulateResultsGrid();

                step1Panel.Visible = false;
                resultsPanel.Visible = true;
                btnSearch.Visible = false;
                btnClearResults.Visible = false;

                GridViewRow row = grdResults.Rows[e.NewEditIndex];

                row.ForeColor = System.Drawing.Color.Red;

                Label description = (Label)row.FindControl("Description");
                txtDescription.Text = description.Text;

                Label resolution = (Label)row.FindControl("Resolution");
                txtResolution.Text = resolution.Text;

                Label dateResolved = (Label)row.FindControl("DateResolved");
                txtDateResolved.Text = dateResolved.Text;

                //tell user what ticket they are updating
                Label lblId = (Label)row.FindControl("ID");
                
                //Label that contains section seperator text
                Label24.Text = "Update Instructions: Update ticket information below...";
                Label24.ForeColor = System.Drawing.Color.Red;


                lblSearchMessage.Text = $"You are about to update ticket #{lblId.Text}. Please proceed to 'Update Instructions' Area below.";
                lblSearchMessage.ForeColor = System.Drawing.Color.Red; //make message red

                ClearLabels(lblSearchMessage);
                lblResults.Text = string.Empty;

            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        protected void grdResults_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ClearLabels(lblResults);

                GridViewRow row = grdResults.Rows[e.RowIndex];
                Label lblId = (Label)row.FindControl("Id");

                int ticketid = Convert.ToInt32(lblId.Text);
                logger.Debug($"Ticket ID: {ticketid}");

                temp = new Ticket(ticketid, "%", -1, -1, -1, -1, "%", DateTime.Now, "%");

                ticketbo.DeleteOneObject(temp);
                PopulateResultsGrid();

                //display message to user -- successful delet
                lblResults.Text = $"You have successffuly deleted ticket #{ticketid}";
                lblResults.ForeColor = System.Drawing.Color.Green; //make message green
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                //display message to user
                lblResults.Text = $"Delete Error!!! Please try again.";
                lblResults.ForeColor = System.Drawing.Color.Red; 
            }
        }

        protected void grdResults_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                logger.Debug($"Update Index: {e.RowIndex}");
                GridViewRow row = grdResults.Rows[e.RowIndex];

                Label lblId = (Label)row.Controls[0].FindControl("Id");
                int ticketId = Convert.ToInt32(lblId.Text);

                string description = Convert.ToString(txtDescription.Text);
                int bugId = Convert.ToInt32(drpBugs.SelectedValue);
                int appId = Convert.ToInt32(drpApps.SelectedValue);
                int statusId = Convert.ToInt32(drpStatuses.SelectedValue);
                string statusName = drpStatuses.SelectedItem.ToString(); //using for logging
                int priorityId = Convert.ToInt32(drpPriorities.SelectedValue);
                string priorityName = drpPriorities.SelectedItem.ToString(); //using for loggin
                string resolution = txtResolution.Text;
                string dateResolved = txtDateResolved.Text;


                //log statements -- see what's about to get updated with
                logger.Debug($"Ticket Id: {ticketId}");
                logger.Debug($"Description: {description}");
                logger.Debug($"Bug Id: {bugId}");
                logger.Debug($"SoftwareApp Id: {appId}");
                logger.Debug($"Selected Status from Dropdown [Update Mode]: {statusId} {statusName}");
                logger.Debug($"Selected Priority from Dropdown [Update Mode]: {priorityId} {priorityName}");
                logger.Debug($"Resolution: {resolution}");
                logger.Debug($"Date Resolved : {dateResolved}");

                temp = new Ticket(ticketId, description, bugId, appId, priorityId, statusId, resolution, DateTime.Now, dateResolved);

                ticketbo.UpdateOneObject(temp);

                grdResults.EditIndex = -1;

                //display message to user -- successful update
                lblResults.Text = $"You have successffuly updated ticket #{ticketId}";
                lblResults.ForeColor = System.Drawing.Color.Green; //make message green
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                //display message to user -- error update
                lblResults.Text = $"Updae Failed. Please try again.";
                lblResults.ForeColor = System.Drawing.Color.Red; //make message green

            }
            finally
            {
                PopulateResultsGrid();
                //reset section seperator text
                Label24.Text = "Step 1: Choice Filter Option";
                Label24.ForeColor = System.Drawing.Color.Black;

                //empty textboxes 
                txtDescription.Text = string.Empty;
                txtResolution.Text = string.Empty;
                txtDateResolved.Text = string.Empty;

                //make panel with filering options visible
                step1Panel.Visible = true;

                //hide panel with resolution and date resolved textboxes for updating
                resultsPanel.Visible = false;

                //reset search button visibility back to true
                btnSearch.Visible = true;
                btnClearResults.Visible = true;

                ClearLabels(lblResults);
            }

        }

        protected void grdResults_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            logger.Debug($"Cancel Index: {e.RowIndex}");
            grdResults.EditIndex = -1;

            lblSearchMessage.Text = "Choose 1 filter to use in your search [Description, Bug, Software Application, Status or Priority] OR Choose to display all tickets.";
            lblSearchMessage.ForeColor = System.Drawing.Color.Green;

            lblResults.Text = "Ticket Update Cancelled";
            lblResults.ForeColor = System.Drawing.Color.Red;

            Label24.Text = "Step 2: Fill out Filter based on option";
            Label24.ForeColor = System.Drawing.Color.Black;
            step1Panel.Visible = true;

            PopulateResultsGrid();
            txtDescription.Text = string.Empty;
            txtResolution.Text = string.Empty;
            txtDateResolved.Text = string.Empty;

            lblStaffMessage.Text = string.Empty;
            lblStatusMessage.Text = string.Empty;
            lblPriorityMessage.Text = string.Empty;
            lblBugMessage.Text = string.Empty;
            lblAppMessage.Text = string.Empty;
        }

        //Label Message Clearer Method
        public void ClearLabels(Label lbl)
        {
            if(lbl.ID == "lblStaffMessage")
            {
                lblStatusMessage.Text = string.Empty;
                lblPriorityMessage.Text = string.Empty;
                lblBugMessage.Text = string.Empty;
                lblAppMessage.Text = string.Empty;
                lblResults.Text = string.Empty;

            }
            if (lbl.ID == "lblStatusMessage")
            {
                lblStaffMessage.Text = string.Empty;
                lblPriorityMessage.Text = string.Empty;
                lblBugMessage.Text = string.Empty;
                lblAppMessage.Text = string.Empty;
                lblResults.Text = string.Empty;

            }
            if (lbl.ID == "lblPriorityMessage")
            {
                lblStaffMessage.Text = string.Empty;
                lblStatusMessage.Text = string.Empty;
                lblBugMessage.Text = string.Empty;
                lblAppMessage.Text = string.Empty;
                lblResults.Text = string.Empty;

            }
            if (lbl.ID == "lblBugMessage")
            {
                lblStaffMessage.Text = string.Empty;
                lblStatusMessage.Text = string.Empty;
                lblPriorityMessage.Text = string.Empty;
                lblAppMessage.Text = string.Empty;
                lblResults.Text = string.Empty;

            }
            if (lbl.ID == "lblAppMessage")
            {
                lblStaffMessage.Text = string.Empty;
                lblStatusMessage.Text = string.Empty;
                lblPriorityMessage.Text = string.Empty;
                lblBugMessage.Text = string.Empty;
                lblResults.Text = string.Empty;

            }
            if (lbl.ID == "lblSearchMessage")
            {
                lblStaffMessage.Text = string.Empty;
                lblStatusMessage.Text = string.Empty;
                lblPriorityMessage.Text = string.Empty;
                lblBugMessage.Text = string.Empty;
                lblAppMessage.Text = string.Empty;
            }
            if (lbl.ID == "lblResults")
            {
                lblStaffMessage.Text = string.Empty;
                lblStatusMessage.Text = string.Empty;
                lblPriorityMessage.Text = string.Empty;
                lblBugMessage.Text = string.Empty;
                lblAppMessage.Text = string.Empty;
                lblSearchMessage.Text = string.Empty;
            }
        }
    }
}