﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Bugs.aspx.cs" Inherits="csis5690project_Morehead.Bugs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large" Text="Bugs"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblSearchMessage" runat="server"></asp:Label>
    </p>



    <asp:Panel ID="pnlBug" runat="server">
        <asp:Label ID="Label3" runat="server" Text="Existing Bug List" Font-Size="Medium"></asp:Label>
        <br />


        <asp:Repeater ID="rptBugs" runat="server">
            <HeaderTemplate>
                <table border ="1">
            </HeaderTemplate>

            <ItemTemplate>
                <tr>
                    <td>
                        <asp:Label ID ="lblId" Text ='<%#Eval("Id") %>' runat="server"></asp:Label>
                    </td>

                    <td>
                        <asp:Label ID="lblName" Text='<%#Eval("Name") %>' runat="server"></asp:Label>
                    </td>

                    <td>
                        <asp:Button ID="btnEdit" Text='Edit' OnClick="btnEdit_Click" runat="server"></asp:Button>
                    </td>

                    <td>
                        <asp:Button ID="btnDelete" Text='Delete' OnClick="btnDelete_Click" runat="server"></asp:Button>
                    </td>
                </tr>
            </ItemTemplate>

            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>


    </asp:Panel>




    <p>
        &nbsp;</p>
    <p>
        <asp:HiddenField ID="hdnBudId" runat="server" />
    </p>
    <p>
        <asp:Label ID="Label2" runat="server" Font-Size="Medium" Text="Bug:"></asp:Label>
&nbsp;<asp:TextBox ID="txtBug" runat="server"></asp:TextBox>
    </p>
    <p>
        <asp:Button ID="btnAddBug" runat="server" OnClick="btnAddBug_Click" Text="Add" Width="68px" />
    </p>
</asp:Content>
