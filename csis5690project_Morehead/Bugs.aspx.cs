﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using csis5690.Domain;
using csis5690.DAL;
using csis5690.BL;

namespace csis5690project_Morehead
{
    public partial class Bugs : System.Web.UI.Page
    {
        private static readonly ILog logger =
           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private BugBO bo;

        Bug temp;

        protected void Page_Load(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();

            bo = new BugBO("localhost");

            if(Page.IsPostBack)
            {
                //not the first time in page
            }
            else
            {
                //first time in page
                PopulateBugRepeator();
            }
        }

        private void PopulateBugRepeator()
        {
            try
            {
                IList<object> bugs = bo.SelectManyObjects(new Bug(-1, "%", DateTime.Now));

                if(bugs.Count > 0)
                {
                    pnlBug.Visible = true;
                    rptBugs.DataSource = bugs;
                    rptBugs.DataBind();
                }
                else
                {
                    lblSearchMessage.Text = "No (Bugs) currently exist. Please add a new (Bug).";
                    lblSearchMessage.ForeColor = System.Drawing.Color.OrangeRed;
                    pnlBug.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //lblSearchMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        protected void btnAddBug_Click(object sender, EventArgs e)
        {
            lblSearchMessage.Text = string.Empty;
            string bugName = txtBug.Text;
            int bugId;

            try
            {
                if (btnAddBug.Text.Equals("Edit"))
                {
                    //doing an update
                    bugId = Convert.ToInt32(hdnBudId.Value);
                    Bug filter = new Bug(bugId, bugName, DateTime.Now);
                    temp = (Bug)bo.UpdateOneObject(filter);
                    lblSearchMessage.Text = "Bug Edited Successfully!!!";
                    btnAddBug.Text = "Add";
                }
                else
                {
                    //doing an insert or add
                    temp = new Bug(-1, bugName, DateTime.Now);
                    temp = (Bug)bo.InsertOneObject(temp);
                    lblSearchMessage.Text = "Bug Added Successfully!!!";
                }

                PopulateBugRepeator();
                hdnBudId.Value = string.Empty;
                txtBug.Text = string.Empty;
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;
                logger.Debug($"BUG: {temp.ToString()}");
            }
            catch (Exception ex)
            {
                //lblSearchMessage.Text = ex.Message;
                //lblSearchMessage.ForeColor = System.Drawing.Color.Red;
                logger.Error(ex);
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                logger.Debug($"From Repeator: Clicked Edit!!!");

                Button btnEdit = (Button)sender;
                logger.Debug($"From Button: {btnEdit.ID}");

                RepeaterItem item = (RepeaterItem)btnEdit.NamingContainer;
                Label idlabel = (Label)item.FindControl("lblId");
                Label namelabel = (Label)item.FindControl("lblName");
                logger.Debug($"Id Label: {idlabel.Text}");
                logger.Debug($"Name Label: {namelabel.Text}");

                int bugId = Convert.ToInt32(idlabel.Text);
                Bug filter = new Bug(bugId, "%", DateTime.Now);
                Bug temp = (Bug)bo.SelectOneObject(filter);

                txtBug.Text = temp.Name;
                hdnBudId.Value = Convert.ToString(bugId);
                btnAddBug.Text = "Edit";
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            logger.Debug($"From Repeator: Clicked Delete!!!");

            Button btnDelete = (Button)sender;
            logger.Debug($"From Button: {btnDelete.ID}");

            RepeaterItem item = (RepeaterItem)btnDelete.NamingContainer;
            Label idlabel = (Label)item.FindControl("lblId");
            Label namelabel = (Label)item.FindControl("lblName");

            logger.Debug($"ID Label: {idlabel.Text}");
            logger.Debug($"Name Label: {namelabel.Text}");

            try
            {
                int bugId = Convert.ToInt32(idlabel.Text);
                temp = new Bug(bugId, "%", DateTime.Now);
                bo.DeleteOneObject(temp);

                logger.Debug($"Bug Deleted: PK: {bugId}");

                lblSearchMessage.Text = "Bug Successfully Deleted";
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;
                PopulateBugRepeator();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            txtBug.Text = string.Empty;
        }
    }
}