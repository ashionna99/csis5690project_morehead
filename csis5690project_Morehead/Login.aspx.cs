﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using csis5690.BL;
using csis5690.Domain;
using log4net.Repository.Hierarchy;
using System.CodeDom;

namespace csis5690project_Morehead
{
    public partial class Login : System.Web.UI.Page
    {
        private static readonly ILog logger =
          log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();

            lblMessage.Text = string.Empty;

            if(Page.IsPostBack)
            {
                //not first time in page
            }
            else
            {
                //first time in page
                txtUsername.Text = string.Empty;
                txtPassword.Text = string.Empty;
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                //if username is blank display message
                if((txtUsername.Text == string.Empty) || (txtUsername.Text == null))
                {
                    lblMessage.Text = "Username cannot be blank";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
                //username not empty
                else
                {
                    //if password is blank display message
                    if ((txtPassword.Text == string.Empty) || (txtPassword.Text == null))
                    {
                        lblMessage.Text = "Password cannot be blank";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                    }
                    //password not empty
                    else
                    {
                        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["localhost"].ToString());

                        string username = txtUsername.Text;
                        string password = txtPassword.Text;

                        con.Open(); //open the connection to the database
                        string query = "SELECT * FROM STAFF WHERE username='" + username + "'AND password='" + password + "';";

                        SqlCommand cmd = new SqlCommand(query, con); //sends query to database for checking

                        SqlDataReader rdr = cmd.ExecuteReader(); //read each entry in database for match


                        if (rdr.Read()) //while reading the database table line by line -- if there is a user with those credentials go ahead and validate their jobcode
                        {
                            //Doing validation by jobcode description rather than ID because if the code is deleted and re-entered the ID changes which causes the page to crash 

                            int jobcode = Convert.ToInt32(rdr["jobcode"].ToString());  //takes value from readin jobcode column and converts into (int)

                            rdr.Close(); //need to close first reader to create and open a 2nd reader

                            string jobCodeQuery = "SELECT Description FROM JOBCODE WHERE ID =" + jobcode;
                            SqlCommand cmd2 = new SqlCommand(jobCodeQuery, con);
                            SqlDataReader rdr2 = cmd2.ExecuteReader();


                            if (rdr2.Read())
                            {
                                string description = rdr2["Description"].ToString();

                                if (description.Equals("Administrator") || description.Equals("administrator"))
                                {
                                    Response.Redirect("Admin.aspx");
                                }
                                else if (description.Equals("Employee") || description.Equals("employee"))
                                {
                                    Response.Redirect("Submission Form.aspx");
                                }
                            }

                        }
                        else
                        {
                            lblMessage.Text = "Invalid Credentials";
                            lblMessage.ForeColor = System.Drawing.Color.Red;
                            txtUsername.Text = string.Empty;
                            txtPassword.Text = string.Empty;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                lblMessage.Text = "Login Error. Please try again.";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}