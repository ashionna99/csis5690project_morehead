﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Priorities.aspx.cs" Inherits="csis5690project_Morehead.Priorities" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large" Text="Priorities"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblSearchMessage" runat="server"></asp:Label>
    </p>



    <asp:Panel ID="pnlPriorities" runat="server" Height="190px">
        <asp:Label ID="Label2" runat="server" Font-Bold="False" Font-Size="Medium" Text="Existing Priority List"></asp:Label>
        <br />


        <asp:Repeater ID="rptPriorities" runat="server">
            <HeaderTemplate>
                <table border ="1">
            </HeaderTemplate>

            <ItemTemplate>
                <tr>
                    <td>
                        <asp:Label ID ="lblId" Text ='<%#Eval("Id") %>' runat="server"></asp:Label>
                    </td>

                    <td>
                        <asp:Label ID="lblName" Text ='<%#Eval("Name") %>' runat="server"></asp:Label>
                    </td>

                    <td>
                        <asp:Button ID="btnEdit" Text ="Edit" OnClick="btnEdit_Click" runat="server"></asp:Button>
                    </td>

                    <td>
                        <asp:Button ID="btnDelete" Text="Delete" OnClick="btnDelete_Click" runat="server"></asp:Button>
                    </td>
            </ItemTemplate>

            <FooterTemplate>
                </table>
            </FooterTemplate>

        </asp:Repeater>




    </asp:Panel>
    <br />
    <asp:HiddenField ID="hdnPriorityId" runat="server" />
    <br />
    <asp:Label ID="Label3" runat="server" Font-Bold="False" Font-Size="Medium" Text="Priority: "></asp:Label>
    <asp:TextBox ID="txtPriority" runat="server"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="btnAddPriority" runat="server" Text="Add" Width="67px" OnClick="btnAddPriority_Click" />
</asp:Content>
