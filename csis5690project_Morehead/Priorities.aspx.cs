﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using csis5690.Domain;
using csis5690.DAL;
using csis5690.BL;

namespace csis5690project_Morehead
{
    public partial class Priorities : System.Web.UI.Page
    {
        private static readonly ILog logger =
           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private PriorityBO bo;

        Priority temp;

        protected void Page_Load(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();

            bo = new PriorityBO("localhost");

            if(Page.IsPostBack)
            {
                //not the first time in page
            }
            else
            {
                //first time in page
                PopulatePriorityRepeator();
            }
        }

        private void PopulatePriorityRepeator()
        {
            try
            {
                IList<object> priorities = bo.SelectManyObjects(new Priority(-1, "%", DateTime.Now));

                if(priorities.Count > 0)
                {
                    pnlPriorities.Visible = true;
                    rptPriorities.DataSource = priorities;
                    rptPriorities.DataBind();
                }
                else
                {
                    lblSearchMessage.Text = "No (Priorities) currently exist. Please ass a new (Priority).";
                    lblSearchMessage.ForeColor = System.Drawing.Color.OrangeRed;
                    pnlPriorities.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblSearchMessage.Text = ex.Message;
            }
        }

        protected void btnAddPriority_Click(object sender, EventArgs e)
        {
            lblSearchMessage.Text = string.Empty;
            string priorityName = txtPriority.Text;
            int priorityId;

            try
            {
                if(btnAddPriority.Text.Equals("Edit"))
                {
                    //doing an update
                    priorityId = Convert.ToInt32(hdnPriorityId.Value);
                    Priority filter = new Priority(priorityId, priorityName, DateTime.Now);
                    temp = (Priority)bo.UpdateOneObject(filter);
                    lblSearchMessage.Text = "Priority Edit Successfully!!!";
                    btnAddPriority.Text = "Add";
                }
                else
                {
                    //doing an insert or add
                    temp = new Priority(-1, priorityName, DateTime.Now);
                    temp = (Priority)bo.InsertOneObject(temp);
                    lblSearchMessage.Text = "Priority Added Successfully!!!";
                }

                PopulatePriorityRepeator();
                hdnPriorityId.Value = string.Empty;
                txtPriority.Text = string.Empty;
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;
                logger.Debug($"Priotiy: {temp.ToString()}");
            }
            catch (Exception ex)
            {
                lblSearchMessage.Text = ex.Message;
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;
                logger.Debug(ex);
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                logger.Debug($"From Repeator: Clicked Edit!!!");

                Button btnEdit = (Button)sender;
                logger.Debug($"From Button: {btnEdit.ID}");

                RepeaterItem item = (RepeaterItem)btnEdit.NamingContainer;
                Label idlabel = (Label)item.FindControl("lblId");
                Label namelabel = (Label)item.FindControl("lblName");
                logger.Debug($"Id Label: {idlabel.Text}");
                logger.Debug($"Name Label: {namelabel.Text}");

                int priorityId = Convert.ToInt32(idlabel.Text);
                Priority filter = new Priority(priorityId, "%", DateTime.Now);
                Priority temp = (Priority)bo.SelectOneObject(filter);

                txtPriority.Text = temp.Name;
                hdnPriorityId.Value = Convert.ToString(priorityId);
                btnAddPriority.Text = "Edit";
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            logger.Debug($"From Repeator: Clicked Delete!!!");

            Button btnDelete = (Button)sender;
            logger.Debug($"From Button: {btnDelete.ID}");

            RepeaterItem item = (RepeaterItem)btnDelete.NamingContainer;
            Label idlabel = (Label)item.FindControl("lblId");
            Label namelabel = (Label)item.FindControl("lblName");

            logger.Debug($"ID Label: {idlabel.Text}");
            logger.Debug($"Name Label: {namelabel.Text}");

            try
            {
                int priorityId = Convert.ToInt32(idlabel.Text);
                temp = new Priority(priorityId, "%", DateTime.Now);
                bo.DeleteOneObject(temp);

                logger.Debug($"Priority Deleted: PK: {priorityId}");

                lblSearchMessage.Text = "Priority Successfully Deleted";
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;
                PopulatePriorityRepeator();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            txtPriority.Text = string.Empty;
        }

    }
}