﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SoftwareApps.aspx.cs" Inherits="csis5690project_Morehead.SoftwareApps" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large" Text="Software Applications"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblSearchMessage" runat="server"></asp:Label>
    </p>




    <asp:Panel ID="pnlApps" runat="server">
        <asp:Label ID="Label3" runat="server" Text="Existing Software Application List" Font-Size="Medium"></asp:Label>
        <br />


        <asp:Repeater ID="rptApp" runat="server">
            <HeaderTemplate>
                <table border ="1">
            </HeaderTemplate>

            <ItemTemplate>
                <tr>
                    <td>
                        <asp:Label ID="lblId" Text='<%#Eval("Id") %>' runat="server"></asp:Label>
                    </td>

                    <td>
                        <asp:Label ID="lblName" Text='<%#Eval("Name") %>' runat="server"></asp:Label>
                    </td>

                    <td>
                        <asp:Button ID="btnEdit" Text='Edit' OnClick="btnEdit_Click" runat="server"></asp:Button>
                    </td>

                    <td>
                        <asp:Button ID="btnDelete" Text='Delete' OnClick="btnDelete_Click" runat="server"></asp:Button>
                    </td>
                </tr>
            </ItemTemplate>

            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>



    </asp:Panel>




    <p>
        &nbsp;</p>
    <p>
        <asp:HiddenField ID="hdnAppId" runat="server" />
    </p>
    <p>
        <asp:Label ID="Label2" runat="server" Font-Size="Medium" Text="Software Application: "></asp:Label>
        <asp:TextBox ID="txtApp" runat="server" Width="173px"></asp:TextBox>
    </p>
    <p>
        <asp:Button ID="btnAddApp" runat="server" Text="Add" Width="75px" OnClick="btnAddApp_Click" />
    </p>
</asp:Content>
