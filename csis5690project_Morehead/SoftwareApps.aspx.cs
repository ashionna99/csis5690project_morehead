﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using csis5690.Domain;
using csis5690.BL;
using csis5690.DAL;

namespace csis5690project_Morehead
{
    public partial class SoftwareApps : System.Web.UI.Page
    {
        private static readonly ILog logger =
           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private SoftwareAppBO bo;

        SoftwareApp temp;

        protected void Page_Load(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();

            bo = new SoftwareAppBO("localhost");

            if(Page.IsPostBack)
            {
                //not the first time in page
            }
            else
            {
                //first time in page
                PopulateAppsRepeator();
            }
        }

        private void PopulateAppsRepeator()
        {
            try
            {
                IList<object> apps = bo.SelectManyObjects(new SoftwareApp(-1, "%", DateTime.Now));
                
                if(apps.Count > 0)
                {
                    pnlApps.Visible = true;
                    rptApp.DataSource = apps;
                    rptApp.DataBind();
                }
                else
                {
                    lblSearchMessage.Text = "No (Software Applications) currently exist. Please add a new (Software Application).";
                    lblSearchMessage.ForeColor = System.Drawing.Color.OrangeRed;
                    pnlApps.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //lblSearchMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        protected void btnAddApp_Click(object sender, EventArgs e)
        {
            try
            {
                lblSearchMessage.Text = string.Empty;
                string appName = txtApp.Text;
                int appId;

                if(btnAddApp.Text.Equals("Edit"))
                {
                    //doing an update
                    appId = Convert.ToInt32(hdnAppId.Value);
                    SoftwareApp filter = new SoftwareApp(appId, appName, DateTime.Now);
                    temp = (SoftwareApp)bo.UpdateOneObject(filter);
                    lblSearchMessage.Text = "Software Application Edit Successfully!!!";
                    btnAddApp.Text = "Add";
                }
                else
                {
                    //doing an insert or add
                    temp = new SoftwareApp(-1, appName, DateTime.Now);
                    temp = (SoftwareApp)bo.InsertOneObject(temp);
                    lblSearchMessage.Text = "Software Application Added Successfully!!!";
                }

                PopulateAppsRepeator();
                hdnAppId.Value = string.Empty;
                txtApp.Text = string.Empty;
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;
                logger.Debug($"Softare Application: {temp.ToString()}");
            }
            catch (Exception ex)
            {
                //lblSearchMessage.Text = ex.Message;
                //lblSearchMessage.ForeColor = System.Drawing.Color.Red;
                logger.Error(ex);
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                logger.Debug($"From Repeator: Clicked Edit!!!");

                Button btnEdit = (Button)sender;
                logger.Debug($"From Button: {btnEdit.ID}");

                RepeaterItem item = (RepeaterItem)btnEdit.NamingContainer;
                Label idlabel = (Label)item.FindControl("lblId");
                Label namelabel = (Label)item.FindControl("lblName");
                logger.Debug($"Id Label: {idlabel.Text}");
                logger.Debug($"Name Label: {namelabel.Text}");

                int appId = Convert.ToInt32(idlabel.Text);
                SoftwareApp filter = new SoftwareApp(appId, "%", DateTime.Now);
                SoftwareApp temp = (SoftwareApp)bo.SelectOneObject(filter);

                txtApp.Text = temp.Name;
                hdnAppId.Value = Convert.ToString(appId);
                btnAddApp.Text = "Edit";
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            logger.Debug($"From Repeator: Clicked Delete!!!");

            Button btnDelete = (Button)sender;
            logger.Debug($"From Button: {btnDelete.ID}");

            RepeaterItem item = (RepeaterItem)btnDelete.NamingContainer;
            Label idlabel = (Label)item.FindControl("lblId");
            Label namelabel = (Label)item.FindControl("lblName");

            logger.Debug($"ID Label: {idlabel.Text}");
            logger.Debug($"Name Label: {namelabel.Text}");

            try
            {
                int appId = Convert.ToInt32(idlabel.Text);
                temp = new SoftwareApp(appId, "%", DateTime.Now);
                bo.DeleteOneObject(temp);

                logger.Debug($"Software Application Deleted: PK: {appId}");

                lblSearchMessage.Text = "Software Application Successfully Deleted!!!";
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;
                PopulateAppsRepeator();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            txtApp.Text = string.Empty;
        }
    }
}