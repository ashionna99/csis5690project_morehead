﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Staffs.aspx.cs" Inherits="csis5690project_Morehead.Staffs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Medium" Text="Staff"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblSearchMessage" runat="server"></asp:Label>
    </p>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </p>
    <p>
        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Username:"></asp:Label>
&nbsp;<asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;<asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Underline="True" Text="Add Job Code"></asp:Label>
    </p>
    <p>
        <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Password:"></asp:Label>
&nbsp;<asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label7" runat="server" Font-Bold="True" Text="Job Code Description:"></asp:Label>
&nbsp;<asp:TextBox ID="txtCodeDescription" runat="server"></asp:TextBox>
&nbsp;<asp:Button ID="btnAddCode" runat="server" OnClick="btnAddCode_Click" Text="Add" Width="65px" />
&nbsp;</p>
    <p>
        <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="Job Code: "></asp:Label>
        <asp:DropDownList ID="drpJobCodes" runat="server">
        </asp:DropDownList>
    &nbsp;<asp:Button ID="btnEditCode" runat="server" OnClick="btnEditCode_Click" Text="Edit" Width="65px" />
    &nbsp;<asp:Button ID="btnDeleteCode" runat="server" OnClick="btnDeleteCode_Click" Text="Delete" Width="71px" />
    </p>
    <p>
        <asp:Button ID="btnAddStaff" runat="server" Text="Add" Width="76px" OnClick="btnAddStaff_Click" />
    </p>
    <p>
        &nbsp;</p>
    <p>
    </p>




    <asp:Panel ID="pnlStaff" runat="server">

        <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="Medium" Text="Existing Staff Members"></asp:Label>
        <br />



        <asp:GridView ID="grdStaffs" runat="server"
            AutoGenerateColumns="false"
            HeaderStyle-HorizontalAlign="Center"
            OnRowEditing="grdStaffs_RowEditing"
            OnRowUpdating="grdStaffs_RowUpdating"
            OnRowCancelingEdit="grdStaffs_RowCancelingEdit"
            OnRowDeleting="grdStaffs_RowDeleting"
            OnRowDataBound="grdStaffs_RowDataBound"
            >

            <Columns>
                <asp:TemplateField HeaderText="ID">
                    <ItemTemplate>
                        <asp:Label ID="Id" runat="server" Text='<%#Eval("Id") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="Username" HeaderText="Username" />
                <asp:BoundField DataField="Password" HeaderText="Password" />

                <asp:TemplateField HeaderText="JobCode">
                    <EditItemTemplate>
                        <asp:DropDownList ID="drpJobCodes" runat="server"></asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="JobCode" runat="server" Text='<%#Eval("JobCode") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:CommandField ShowEditButton ="true" />
                <asp:CommandField ShowDeleteButton ="true" />
            </Columns>

        </asp:GridView>




    </asp:Panel>







    <p>
        &nbsp;</p>
    <p>
        <asp:HiddenField ID="hdnStaffId" runat="server" />
    </p>
    <p>
        <asp:HiddenField ID="hdnCodeId" runat="server" />
    </p>
</asp:Content>
