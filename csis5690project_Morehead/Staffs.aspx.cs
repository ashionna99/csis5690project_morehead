﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using csis5690.Domain;
using csis5690.DAL;
using csis5690.BL;

namespace csis5690project_Morehead
{
    public partial class Staffs : System.Web.UI.Page
    {
        private static readonly ILog logger =
           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private StaffBO bo;
        private JobCodeBO codebo;

        Staff temp;
        JobCode codetemp;

        protected void Page_Load(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();

            bo = new StaffBO("localhost");
            codebo = new JobCodeBO("localhost");

            if (Page.IsPostBack)
            {
                //not the first time in page
            }
            else
            {
                //first time in page
                PopulateJobCodeDropDown();
                PopulateStaffGridView();
            }
        }

        //Dropdown and Gridview Items
        private void PopulateJobCodeDropDown()
        {
            try
            {
                drpJobCodes.DataSource = codebo.SelectManyObjects(new JobCode(-1, "%"));
                drpJobCodes.DataValueField = "Id";
                drpJobCodes.DataTextField = "Description";
                drpJobCodes.DataBind();
            }
            catch (Exception ex)
            {
                //lblSearchMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        private void PopulateStaffGridView()
        {
            try
            {
                IList<object> staffs = bo.SelectManyObjects(new Staff(-1, "%", "%", -1));

                if(staffs.Count > 0)
                {
                    pnlStaff.Visible = true;
                    grdStaffs.DataSource = staffs;
                    grdStaffs.DataBind();
                }
                else
                {
                    lblSearchMessage.Text = "No (Staff Members) currently exist. Please add a (Staff Member).";
                    lblSearchMessage.ForeColor = System.Drawing.Color.OrangeRed;
                    pnlStaff.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //lblSearchMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }


        //Staff Items
        protected void btnAddStaff_Click(object sender, EventArgs e)
        {
            lblSearchMessage.Text = string.Empty;
            string staffUsername = txtUsername.Text;
            string staffPassword = txtPassword.Text;
            int jobCode = Convert.ToInt32(drpJobCodes.SelectedValue);
            int staffId;

            try
            {
                if(btnAddStaff.Text.Equals("Edit"))
                {
                    //doing an update
                    staffId = Convert.ToInt32(hdnStaffId.Value);
                    Staff filter = new Staff(staffId, staffUsername, staffPassword, jobCode);
                    temp = (Staff)bo.UpdateOneObject(filter);
                    lblSearchMessage.Text = "Staff Edited Successfully!!!";
                    btnAddStaff.Text = "Add";
                }
                else
                {
                    //doing an insert or add
                    temp = new Staff(-1, staffUsername, staffPassword, jobCode);
                    temp = (Staff)bo.InsertOneObject(temp);
                    lblSearchMessage.Text = "Staff Added Successfully!!!";
                }
                PopulateStaffGridView();
                hdnStaffId.Value = string.Empty;
                txtUsername.Text = string.Empty;
                txtPassword.Text = string.Empty;
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;
                logger.Debug($"Staff: {temp.ToString()}");
            }
            catch (Exception ex)
            {
                //lblSearchMessage.Text = ex.Message;
                //lblSearchMessage.ForeColor = System.Drawing.Color.Red;
                logger.Error(ex);
            }
        }

        protected void grdStaffs_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdStaffs.EditIndex = e.NewEditIndex;
            PopulateStaffGridView();
        }

        protected void grdStaffs_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = grdStaffs.Rows[e.RowIndex];

            Label lblId = (Label)row.FindControl("Id");
            TextBox txtUsername = (TextBox)row.Cells[1].Controls[0];
            TextBox txtPassword = (TextBox)row.Cells[2].Controls[0];
            DropDownList drpJobCodes = (row.FindControl("drpJobCodes") as DropDownList);
            int jobCode = Convert.ToInt32(drpJobCodes.SelectedValue);

            logger.Debug($"Job Code (Selected Value): {drpJobCodes.SelectedValue}"); //ID
            logger.Debug($"Job Code (Selected Index): {drpJobCodes.SelectedIndex}"); //INDEX
            logger.Debug($"Job Code (Selected Item): {drpJobCodes.SelectedItem}"); //NAME 
            
            
            int staffId = Convert.ToInt32(lblId.Text);
            Staff temp = new Staff(staffId, txtUsername.Text, txtPassword.Text, jobCode);

            bo.UpdateOneObject(temp);
            grdStaffs.EditIndex = -1;
            PopulateStaffGridView();
            
        }

        protected void grdStaffs_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdStaffs.EditIndex = -1;
            PopulateStaffGridView();
        }

        protected void grdStaffs_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = grdStaffs.Rows[e.RowIndex];
            Label lblId = (Label)row.FindControl("Id");
            //string staffId = row.Cells[0].Text;
            int id = Convert.ToInt32(lblId.Text);

            temp = new Staff(id, "%", "%", -1);
            bo.DeleteOneObject(temp);
            PopulateStaffGridView();
        }

        protected void grdStaffs_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList drpJobCodes = (e.Row.FindControl("drpJobCodes") as DropDownList);
                    drpJobCodes.DataSource = codebo.SelectManyObjects(new JobCode(-1, "%"));
                    drpJobCodes.DataValueField = "Id";
                    drpJobCodes.DataTextField = "Description";
                    drpJobCodes.DataBind();
                }
            }
            catch (Exception ex)
            {
                //lblSearchMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }



        //Job Code Items
        protected void btnAddCode_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnAddCode.Text == "Update")
                {
                    //doing an update
                    int codeId = Convert.ToInt32(drpJobCodes.SelectedValue);
                    string codeDescription = txtCodeDescription.Text;

                    JobCode filter = new JobCode(codeId, codeDescription);
                    codetemp = (JobCode)codebo.UpdateOneObject(filter);

                    lblSearchMessage.Text = "Job Code Edited Successfully!!!";
                    lblSearchMessage.ForeColor = System.Drawing.Color.Green;

                    txtCodeDescription.Text = string.Empty;
                    btnEditCode.Visible = true;
                    btnAddCode.Text = "Add";
                }
                else
                {
                    lblSearchMessage.Text = string.Empty;
                    string codeDescription = txtCodeDescription.Text;

                    //doing an insert or add
                    codetemp = new JobCode(-1, codeDescription);
                    codetemp = (JobCode)codebo.InsertOneObject(codetemp);
                    lblSearchMessage.Text = "Job Code Added Successfully";

                    txtCodeDescription.Text = string.Empty;
                    hdnCodeId.Value = string.Empty;
                    lblSearchMessage.ForeColor = System.Drawing.Color.Green;
                    logger.Debug($"Job Code: {codetemp.ToString()}");
                }
            }
            catch (Exception ex)
            {
                //lblSearchMessage.Text = ex.Message;
                //lblSearchMessage.ForeColor = System.Drawing.Color.Red;
                logger.Error(ex);
            }
            PopulateJobCodeDropDown();
        }

        protected void btnEditCode_Click(object sender, EventArgs e)
        {
            txtCodeDescription.Text = Convert.ToString(drpJobCodes.SelectedItem);
            btnAddCode.Text = "Update";
            btnDeleteCode.Visible = true;
            btnEditCode.Visible = false;
        
            PopulateJobCodeDropDown();
        }

        protected void btnDeleteCode_Click(object sender, EventArgs e)
        {
            int codeId = Convert.ToInt32(drpJobCodes.SelectedValue);

            JobCode filter = new JobCode(codeId, "%");
            codetemp = (JobCode)codebo.DeleteOneObject(filter);

            lblSearchMessage.Text = "Job Code Deleted Successfully!!!";
            lblSearchMessage.ForeColor = System.Drawing.Color.Green;

            txtCodeDescription.Text = string.Empty;
            btnEditCode.Visible = true;
            btnDeleteCode.Visible = false;

            PopulateJobCodeDropDown();
        }

    } //end of class,page
}//end of namespace