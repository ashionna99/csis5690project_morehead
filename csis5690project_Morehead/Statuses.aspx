﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Statuses.aspx.cs" Inherits="csis5690project_Morehead.Statuses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large" Text="Statuses"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblSearchMessage" runat="server"></asp:Label>
    </p>




    <asp:Panel ID="pnlStatuses" runat="server">

        <asp:Label runat="server" Text="Existing Status List" Font-Size="Medium"></asp:Label>
        <br />

        &nbsp;&nbsp;<asp:Repeater ID="rptStatuses" runat="server">
            <HeaderTemplate>
                <table border ="1">                
            </HeaderTemplate>

            <ItemTemplate>
                <tr>
                    <td>
                        <asp:Label ID = "lblId" Text ='<%#Eval("Id") %>' runat="server"></asp:Label>
                    </td>

                    <td>
                        <asp:Label ID ="lblName" Text ='<%#Eval("Name") %>' runat="server"></asp:Label>
                    </td>

                    <td>
                        <asp:Button ID="btnEdit" Text='Edit' OnClick="btnEdit_Click" runat="server"></asp:Button>
                    </td>

                    <td>
                        <asp:Button ID ="btnDelete" Text ='Delete' OnClick="btnDelete_Click" runat="server"></asp:Button>
                    </td>
               </tr>
            </ItemTemplate>

            <FooterTemplate>
                </table>
            </FooterTemplate>
            
        </asp:Repeater>
        <br />




        <br />
    </asp:Panel>




    <br />
    <asp:HiddenField ID="hdnStatusId" runat="server" />
    <br />
    <asp:Label ID="Label2" runat="server" Font-Size="Medium" Text="Status: "></asp:Label>
    <asp:TextBox ID="txtStatus" runat="server"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="btnAddStatus" runat="server" Text="Add" Width="68px" OnClick="btnAddStatus_Click" />
</asp:Content>
