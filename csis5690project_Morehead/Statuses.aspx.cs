﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using csis5690.Domain;
using csis5690.DAL;
using csis5690.BL;

namespace csis5690project_Morehead
{
    public partial class Statuses : System.Web.UI.Page
    {
        private static readonly ILog logger =
           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private StatusBO bo;

        Status temp;

        protected void Page_Load(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();

            bo = new StatusBO("localhost");

            if(Page.IsPostBack)
            {
                //not the first time in page
            }
            else
            {
                //first time in page
                PopulateStatusRepeator();
            }
        }

        private void PopulateStatusRepeator()
        {
            try
            {
                IList<object> statuses = bo.SelectManyObjects(new Status(-1, "%", DateTime.Now));

                if(statuses.Count > 0)
                {
                    pnlStatuses.Visible = true;
                    rptStatuses.DataSource = statuses;
                    rptStatuses.DataBind();
                }
                else
                {
                    lblSearchMessage.Text = "No (Statuses) currently exist. Please add a new (Status).";
                    lblSearchMessage.ForeColor = System.Drawing.Color.OrangeRed;
                    pnlStatuses.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //lblSearchMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        protected void btnAddStatus_Click(object sender, EventArgs e)
        {
            lblSearchMessage.Text = string.Empty;
            string statusName = txtStatus.Text;
            int statusId;

            try
            {
                if(btnAddStatus.Text.Equals("Edit"))
                {
                    //doing an update
                    statusId = Convert.ToInt32(hdnStatusId.Value);
                    Status filter = new Status(statusId, statusName, DateTime.Now);
                    temp = (Status)bo.UpdateOneObject(filter);
                    lblSearchMessage.Text = "Status Edited Successfully!!!";
                    btnAddStatus.Text = "Add";
                }
                else
                {
                    //doing an insert or add
                    temp = new Status(-1, statusName, DateTime.Now);
                    temp = (Status)bo.InsertOneObject(temp);
                    lblSearchMessage.Text = "Status Added Successfully!!!";
                }

                PopulateStatusRepeator();
                hdnStatusId.Value = string.Empty;
                txtStatus.Text = string.Empty;
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;
                logger.Debug($"STATUS: {temp.ToString()}");
            }
            catch (Exception ex)
            {
                //lblSearchMessage.Text = ex.Message;
                //lblSearchMessage.ForeColor = System.Drawing.Color.Red;
                logger.Error(ex);
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                logger.Debug($"From Repeator: Clicked Edit!!!");

                Button btnEdit = (Button)sender;
                logger.Debug($"From Button: {btnEdit.ID}");

                RepeaterItem item = (RepeaterItem)btnEdit.NamingContainer;
                Label idlabel = (Label)item.FindControl("lblId");
                Label namelabel = (Label)item.FindControl("lblName");
                logger.Debug($"Id Label: {idlabel.Text}");
                logger.Debug($"Name Label: {namelabel.Text}");

                int statusId = Convert.ToInt32(idlabel.Text);
                Status filter = new Status(statusId, "%", DateTime.Now);
                Status temp = (Status)bo.SelectOneObject(filter);

                txtStatus.Text = temp.Name;
                hdnStatusId.Value = Convert.ToString(statusId);
                btnAddStatus.Text = "Edit";
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            logger.Debug($"From Repeator: Clicked Delete!!!");

            Button btnDelete = (Button)sender;
            logger.Debug($"From Button: {btnDelete.ID}");

            RepeaterItem item = (RepeaterItem)btnDelete.NamingContainer;
            Label idlabel = (Label)item.FindControl("lblId");
            Label namelabel = (Label)item.FindControl("lblName");

            logger.Debug($"ID Label: {idlabel.Text}");
            logger.Debug($"Name Label: {namelabel.Text}");

            try
            {
                int statusId = Convert.ToInt32(idlabel.Text);
                temp = new Status(statusId, "%", DateTime.Now);
                bo.DeleteOneObject(temp);

                logger.Debug($"Status Deleted: PK: {statusId}");
                
                lblSearchMessage.Text = "Status Successfully Deleted";
                lblSearchMessage.ForeColor = System.Drawing.Color.Green;
                PopulateStatusRepeator();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            txtStatus.Text = string.Empty;
        }
    }
}