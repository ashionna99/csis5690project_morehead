﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Submission Form.aspx.cs" Inherits="csis5690project_Morehead.Tickets" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large" Text="Helpdesk Ticket Submission Form"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblSearchMessage" runat="server"></asp:Label>
    </p>
    <p>
        <asp:Label ID="Label2" runat="server" Font-Size="Medium" Text="Description:"></asp:Label>
&nbsp;<asp:TextBox ID="txtDescription" runat="server" Height="25px" Width="361px"></asp:TextBox>
    </p>
    <p>
        <asp:Label ID="Label3" runat="server" Font-Size="Medium" Text="Bug: "></asp:Label>
        <asp:DropDownList ID="drpBugs" runat="server">
        </asp:DropDownList>
    </p>
    <p>
        <asp:Label ID="Label4" runat="server" Font-Size="Medium" Text="Software Application:"></asp:Label>
&nbsp;<asp:DropDownList ID="drpApps" runat="server">
        </asp:DropDownList>
    </p>
    <p>
        <asp:Label ID="Label5" runat="server" Font-Size="Medium" Text="Priority: "></asp:Label>
        <asp:DropDownList ID="drpPriorities" runat="server">
        </asp:DropDownList>
    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Button ID="btnSubmitTicket" runat="server" Font-Size="Medium" OnClick="btnSubmitTicket_Click" Text="Submit" Width="83px" />
    </p>
    <p>
        &nbsp;</p>
    </asp:Content>
