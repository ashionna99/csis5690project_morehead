﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using csis5690.Domain;
using csis5690.DAL;
using csis5690.BL;

namespace csis5690project_Morehead
{
    public partial class Tickets : System.Web.UI.Page
    {
        private static readonly ILog logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private TicketBO bo;
        private BugBO bugbo;
        private StatusBO statusbo;
        private PriorityBO prioritybo;
        private SoftwareAppBO appbo;

        Ticket temp;


        protected void Page_Load(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();

            bo = new TicketBO("localhost");
            bugbo = new BugBO("localhost");
            statusbo = new StatusBO("localhost");
            prioritybo = new PriorityBO("localhost");
            appbo = new SoftwareAppBO("localhost");

            if (Page.IsPostBack)
            {
                //not the first time into page

            }
            else
            {
                //first time into page
                PopulateBugDropDown();
                PopulateSoftwareAppDropDown();
                PopulatePriorityDropDown();
                //PopulateStatusDropDown();
            }
        }

        private void PopulateBugDropDown()
        {
            try
            {
                IList<object> bugs = bugbo.SelectManyObjects(new Bug(-1, "%", DateTime.Now));

                drpBugs.DataSource = bugs;
                drpBugs.DataValueField = "Id";
                drpBugs.DataTextField = "Name";
                drpBugs.DataBind();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        private void PopulateSoftwareAppDropDown()
        {
            try
            {
                IList<object> apps = appbo.SelectManyObjects(new SoftwareApp(-1, "%", DateTime.Now));

                drpApps.DataSource = apps;
                drpApps.DataValueField = "Id";
                drpApps.DataTextField = "Name";
                drpApps.DataBind();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        private void PopulatePriorityDropDown()
        {
            try
            {
                IList<object> priorities = prioritybo.SelectManyObjects(new Priority(-1, "%", DateTime.Now));

                drpPriorities.DataSource = priorities;
                drpPriorities.DataValueField = "Id";
                drpPriorities.DataTextField = "Name";
                drpPriorities.DataBind();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        /*
        private void PopulateStatusDropDown()
        {
            try
            {
                IList<object> statuses = statusbo.SelectManyObjects(new Status(-1, "%", DateTime.Now));

                drpStatuses.DataSource = statuses;
                drpStatuses.DataValueField = "Id";
                drpStatuses.DataTextField = "Name";
                drpStatuses.DataBind();
            }
            catch (Exception ex)
            {
                logger.Debug(ex);
            }
        }
        */
       
        protected void btnSubmitTicket_Click(object sender, EventArgs e)
        {
            try
            {
                lblSearchMessage.Text = string.Empty;
                string ticketDescription = txtDescription.Text;
                //string ticketResolution = txtResolution.Text;
                //string dateResolved = txtDateResolved.Text;

                //doing an insert or submission
                temp = new Ticket(-1, ticketDescription, Convert.ToInt32(drpBugs.SelectedValue), Convert.ToInt32(drpApps.SelectedValue), Convert.ToInt32(drpPriorities.SelectedValue), -1, "%", DateTime.Now, "%");
                temp = (Ticket)bo.InsertOneObject(temp);
                lblSearchMessage.Text = "Ticket Successfully Submitted";

                lblSearchMessage.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception ex)
            {
                //lblSearchMessage.Text = ex.Message;
                //lblSearchMessage.ForeColor = System.Drawing.Color.Red;
                logger.Error(ex);
            }

            txtDescription.Text = string.Empty;
            //txtResolution.Text = string.Empty;
        }

    }
}