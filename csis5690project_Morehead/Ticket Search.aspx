﻿<%@ Page Title="" 
    Language="C#" 
    MasterPageFile="~/Site.Master"
    AutoEventWireup="true" 
    CodeBehind="Ticket Search.aspx.cs" 
    Inherits="csis5690project_Morehead.Ticket_Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large" Text="Ticket Search"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Size="Medium" Font-Underline="True" Text="Step 1: Choice Filter Option"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Size="Medium" Text="OR"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label13" runat="server" Font-Bold="True" Font-Size="Medium" Font-Underline="True" Text="Display All Tickets"></asp:Label>
    </p>
    <p>
        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Medium" Text="Filter Option: "></asp:Label>
        

        <asp:DropDownList ID="drpFilter" runat="server">
            <asp:ListItem Selected="True">Description</asp:ListItem>
            <asp:ListItem>Bug</asp:ListItem>
            <asp:ListItem>Software Application</asp:ListItem>
            <asp:ListItem>Status</asp:ListItem>
            <asp:ListItem>Priority</asp:ListItem>
        </asp:DropDownList>


    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnDisplayAll" runat="server" Font-Bold="False" Font-Size="Medium" OnClick="btnDisplayAll_Click" Text="Display All" />


    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Size="Medium" Font-Underline="True" Text="Step 2: Fill Out Filter based on Option"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


    </p>
    <p>
        <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Medium" Text="Description: "></asp:Label>
        <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </p>
    <p>
        <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="Medium" Text="Bug:"></asp:Label>
&nbsp;<asp:DropDownList ID="drpBug" runat="server">
        </asp:DropDownList>
    </p>
    <p>
        <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Size="Medium" Text="Software Application:"></asp:Label>
&nbsp;<asp:DropDownList ID="drpApp" runat="server">
        </asp:DropDownList>
    </p>
    <p>
        <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="Medium" Text="Status: "></asp:Label>
        <asp:DropDownList ID="drpStatus" runat="server">
        </asp:DropDownList>
    </p>
    <p>
        <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="Medium" Text="Priority: "></asp:Label>
        <asp:DropDownList ID="drpPriority" runat="server">
        </asp:DropDownList>
    </p>
    <p>
        <asp:Button ID="btnSearch" runat="server" Font-Size="Medium" Text="Search" OnClick="btnSearch_Click" />
    </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label runat="server" Font-Bold="True" Font-Size="Medium">Results</asp:Label>
    </p>
    <p>
        <asp:Label ID="lblResults" runat="server" Font-Bold="False" Font-Size="Small"></asp:Label>
    </p>
    <p>


        <asp:GridView ID="grdResults" runat="server" 
            AutoGenerateColumns="false" 
            OnRowEditing="grdResults_RowEditing" 
            OnRowUpdating="grdResults_RowUpdating" 
            OnRowCancelingEdit="grdResults_RowCancelingEdit" 
            OnRowDeleting="grdResults_RowDeleting"
            OnRowDataBound="grdResults_RowDataBound"
            >

            <Columns>
              <asp:TemplateField HeaderText="ID">
                  <ItemTemplate>
                      <asp:Label ID="Id" runat="server" Text='<%#Eval("Id") %>'></asp:Label>
                  </ItemTemplate>
              </asp:TemplateField>

                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:Label ID="Description" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Bug">
                    <ItemTemplate>
                        <asp:Label ID="Bug" runat="server" Text='<%#Eval("Bug.Name") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="BugID" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="BugId" runat="server" Text='<%#Eval("Bug.Id") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="SoftwareApp">
                    <ItemTemplate>
                        <asp:Label ID="SoftwareApp" runat="server" Text='<%#Eval("SoftwareApp.Name") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="SoftwareAppID" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="SoftwareAppId" runat="server" Text='<%#Eval("SoftwareApp.Id") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Status">
                    <EditItemTemplate>
                        <asp:DropDownList ID="drpStatuses" runat="server"></asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Status" runat="server" Text='<%#Eval("Status.Name") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

              <asp:BoundField DataField="Status.Name" HeaderText="Status"  Visible="false"/>


                 <asp:TemplateField HeaderText="Priority">
                    <EditItemTemplate>
                        <asp:DropDownList ID="drpPriorities" runat="server"></asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Priority" runat="server" Text='<%#Eval("Priority.Name") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

              <asp:BoundField DataField="Priority.Name" HeaderText="Priority" Visible="false" />


                <asp:TemplateField HeaderText="Date Created">
                    <ItemTemplate>
                        <asp:Label ID="DateCreated" runat="server" Text='<%#Eval("DateCreated") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
              
                <asp:TemplateField HeaderText="Resolution">
                    <ItemTemplate>
                        <asp:Label ID="Resolution" runat="server" Text='<%#Eval("Resolution") %>' ></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID ="txtResolution" runat="server" Text='<%#Eval("Resolution") %>'></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Date Resolved">
                    <ItemTemplate>
                        <asp:Label ID="DateResolved" runat="server" Text='<%#Eval("DateResolved") %>' ></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID ="txtDateResolved" runat="server" Text='<%#Eval("DateResolved") %>'></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>


              <asp:CommandField ShowEditButton="true" />
              <asp:CommandField ShowDeleteButton="true" />
            </Columns>

        </asp:GridView>



    </p>
    <p>



    </p>
    <asp:Panel ID="pnlClearResults" runat="server">
        <asp:Button ID="btnClearResults" runat="server" Font-Size="Medium" OnClick="btnClearResults_Click" Text="Clear Results" Width="142px" />
    </asp:Panel>
    <p>


        &nbsp;</p>
</asp:Content>
