﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using csis5690.Domain;
using csis5690.DAL;
using csis5690.BL;

namespace csis5690project_Morehead
{
    public partial class Ticket_Search : System.Web.UI.Page
    {
        private static readonly ILog logger =
                 log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private BugBO bugbo;
        private SoftwareAppBO appbo;
        private StatusBO statusbo;
        private PriorityBO prioritybo;
        private TicketBO ticketbo;

        Ticket temp;

        protected void Page_Load(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();

            bugbo = new BugBO("localhost");
            appbo = new SoftwareAppBO("localhost");
            statusbo = new StatusBO("localhost");
            prioritybo = new PriorityBO("localhost");
            ticketbo = new TicketBO("localhost");

            if(Page.IsPostBack)
            {
                //not the first time in page
            }
            else
            {
                //first time in page
                pnlResults.Visible = false;
                PopulateDataControls();
                lblMessage.Text = "Choose 1 filter to use in your search [Description, Bug, Software Application, Status or Priority] OR Choose to display all tickets.";
                lblMessage.ForeColor = System.Drawing.Color.Green;
            }
        }

        //Dropdowns
        private void PopulateDataControls()
        {
            PopulateBugDropdown();
            PopulateAppDropdown();
            PopulateStatusDropdown();
            PopulatePriorityDropdown();
        }

        private void PopulateBugDropdown()
        {
            try
            {
                drpBug.DataSource = bugbo.SelectManyObjects(new Bug(-1, "%", DateTime.Now));
                drpBug.DataValueField = "Id";
                drpBug.DataTextField = "Name";
                drpBug.DataBind();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        private void PopulateAppDropdown()
        {
            try
            {
                drpApp.DataSource = appbo.SelectManyObjects(new SoftwareApp(-1, "%", DateTime.Now));
                drpApp.DataValueField = "Id";
                drpApp.DataTextField = "Name";
                drpApp.DataBind();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        private void PopulateStatusDropdown()
        {
            try
            {
                drpStatus.DataSource = statusbo.SelectManyObjects(new Status(-1, "%", DateTime.Now));
                drpStatus.DataValueField = "Id";
                drpStatus.DataTextField = "Name";
                drpStatus.DataBind();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        private void PopulatePriorityDropdown()
        {
            try
            {
                drpPriority.DataSource = prioritybo.SelectManyObjects(new Priority(-1, "%", DateTime.Now));
                drpPriority.DataValueField = "Id";
                drpPriority.DataTextField = "Name";
                drpPriority.DataBind();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

        private void PopulateResultsGrid()
        {
            try
            {
                IList<object> results = ticketbo.SelectManyObjects(new Ticket(-1, "%", -1, -1, -1, -1, "%", DateTime.Now, "%"));

                if (results.Count > 0)
                {
                    grdResults.DataSource = results;
                    grdResults.DataBind();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }


        //Ticket filtering and Gridview Items
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            lblResults.Text = string.Empty;

            if (drpFilter.Text == "Description")
            {
                try //if description textbox has a value
                {
                    //txtDateCreated.Text = string.Empty;
                    //txtDateResolved.Text = string.Empty;

                    logger.Debug($"Description Search: {txtDescription.Text}");

                    lblMessage.Text = "Searched using 'Description' Filter";
                    lblMessage.ForeColor = System.Drawing.Color.Green;

                    Ticket filter = new Ticket(-1, txtDescription.Text.Trim(), -1, -1, -1, -1, "%", DateTime.Now, "%");

                    IList<object> lstResults = ticketbo.SelectManyByDescription(filter);

                    if (lstResults.Count <= 0)
                    {
                        lblResults.Text = "No Results Found";
                        lblResults.ForeColor = System.Drawing.Color.Red;
                        grdResults.DataSource = null;
                        grdResults.DataBind();
                    }
                    else
                    {
                        lblResults.Text = string.Empty;
                        grdResults.DataSource = lstResults;
                        grdResults.DataBind();
                        pnlResults.Visible = true;
                    }
                }
                catch (Exception ex) //if description textbox is blank catch here
                {
                    logger.Error(ex);
                    lblMessage.Text = "Description cannot be blank.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
                
            }
            if (drpFilter.Text == "Bug")
            {
                txtDescription.Text = string.Empty;
                //txtDateCreated.Text = string.Empty;
                //txtDateResolved.Text = string.Empty;

                logger.Debug($"ID: {drpBug.SelectedValue} NAME: {drpBug.SelectedItem}");

                lblMessage.Text = "Searched using 'Bug' Filter";
                lblMessage.ForeColor = System.Drawing.Color.Green;

                Ticket filter = new Ticket(-1, "%", Convert.ToInt32(drpBug.SelectedValue), -1, -1, -1, "%", DateTime.Now, "%");

                IList<object> lstResults = ticketbo.SelectManyByBug(filter);

                if (lstResults.Count <= 0)
                {
                    lblResults.Text = "No Results Found";
                    lblResults.ForeColor = System.Drawing.Color.Red;
                    grdResults.DataSource = null;
                    grdResults.DataBind();
                }
                else
                {
                    lblResults.Text = string.Empty;
                    grdResults.DataSource = lstResults;
                    grdResults.DataBind();
                    pnlResults.Visible = true;
                }
            }
            if (drpFilter.Text == "Software Application")
            {
                txtDescription.Text = string.Empty;
                //txtDateCreated.Text = string.Empty;
                //txtDateResolved.Text = string.Empty;

                logger.Debug($"ID: {drpApp.SelectedValue} NAME: {drpApp.SelectedItem}");

                lblMessage.Text = "Searched using 'Software Application' Filter";
                lblMessage.ForeColor = System.Drawing.Color.Green;

                Ticket filter = new Ticket(-1, "%", -1, Convert.ToInt32(drpApp.SelectedValue), -1, -1, "%", DateTime.Now, "%");

                IList<object> lstResults = ticketbo.SelectManyBySoftwareApp(filter);

                if (lstResults.Count <= 0)
                {
                    lblResults.Text = "No Results Found";
                    lblResults.ForeColor = System.Drawing.Color.Red;
                    grdResults.DataSource = null;
                    grdResults.DataBind();
                }
                else
                {
                    lblResults.Text = string.Empty;
                    grdResults.DataSource = lstResults;
                    grdResults.DataBind();
                    pnlResults.Visible = true;
                }
            }
            if (drpFilter.Text == "Status")
            {
                txtDescription.Text = string.Empty;
                //txtDateCreated.Text = string.Empty;
                //txtDateResolved.Text = string.Empty;

                logger.Debug($"ID: {drpStatus.SelectedValue} NAME: {drpStatus.SelectedItem}");

                lblMessage.Text = "Searched using 'Status' Filter";
                lblMessage.ForeColor = System.Drawing.Color.Green;

                Ticket filter = new Ticket(-1,"%", -1, -1, -1, Convert.ToInt32(drpStatus.SelectedValue), "%", DateTime.Now, "%");

                IList<object> lstResults = ticketbo.SelectManyByStatus(filter);

                if (lstResults.Count <= 0)
                {
                    lblResults.Text = "No Results Found";
                    lblResults.ForeColor = System.Drawing.Color.Red;
                    grdResults.DataSource = null;
                    grdResults.DataBind();
                }
                else
                {
                    lblResults.Text = string.Empty;
                    grdResults.DataSource = lstResults;
                    grdResults.DataBind();
                    pnlResults.Visible = true;
                }
            }
            if (drpFilter.Text == "Priority")
            {
                txtDescription.Text = string.Empty;
                //txtDateCreated.Text = string.Empty;
                //txtDateResolved.Text = string.Empty;

                logger.Debug($"ID: {drpPriority.SelectedValue} NAME: {drpPriority.SelectedItem}");

                lblMessage.Text = "Searched using 'Priority' Filter";
                lblMessage.ForeColor = System.Drawing.Color.Green;

                Ticket filter = new Ticket(-1, "%", -1, -1, Convert.ToInt32(drpPriority.SelectedValue), -1, "%", DateTime.Now, "%");

                IList<object> lstResults = ticketbo.SelectManyByPriority(filter);

                if (lstResults.Count <= 0)
                {
                    lblResults.Text = "No Results Found";
                    lblResults.ForeColor = System.Drawing.Color.Red;
                    grdResults.DataSource = null;
                    grdResults.DataBind();
                }
                else
                {
                    lblResults.Text = string.Empty;
                    grdResults.DataSource = lstResults;
                    grdResults.DataBind();
                    pnlResults.Visible = true;
                }
            }
            /*
            if (drpFilter.Text == "Date Created")
            {
                txtDescription.Text = string.Empty;
                txtDateResolved.Text = string.Empty;

                logger.Debug($"Date Created Search: {txtDateCreated.Text}");

                lblMessage.Text = "Search using 'Date Created' Filter";
                lblMessage.ForeColor = System.Drawing.Color.Green;

                Ticket filter = new Ticket(-1, "%", -1, -1, -1, -1, "%", Convert.ToDateTime(txtDateCreated.Text), "%");

                IList<object> lstResults = ticketbo.SelectManyByDateCreated(filter);

                if (lstResults.Count <= 0)
                {
                    lblResults.Text = "No Results Found";
                    lblResults.ForeColor = System.Drawing.Color.Red;
                    grdResults.DataSource = null;
                    grdResults.DataBind();
                }
                else
                {
                    lblResults.Text = string.Empty;
                    grdResults.DataSource = lstResults;
                    grdResults.DataBind();
                }
            }
            //if (drpFilter.Text == "Date Resolved")
            {
                txtDescription.Text = string.Empty;
                txtDateCreated.Text = string.Empty;

                logger.Debug($"Date Resolved Search: {txtDateResolved.Text}");

                lblMessage.Text = "Search using 'Date Resolved' Filter";
                lblMessage.ForeColor = System.Drawing.Color.Green;

                Ticket filter = new Ticket(-1, "%", -1, -1, -1, -1, "%", DateTime.Now, txtDateResolved.Text.Trim());

                IList<object> lstResults = ticketbo.SelectManyByDateResolved(filter);

                if (lstResults.Count <= 0)
                {
                    lblResults.Text = "No Results Found";
                    lblResults.ForeColor = System.Drawing.Color.Red;
                    grdResults.DataSource = null;
                    grdResults.DataBind();
                }
                else
                {
                    lblResults.Text = string.Empty;
                    grdResults.DataSource = lstResults;
                    grdResults.DataBind();
                }
            }
            */
        }
        
        protected void btnDisplayAll_Click(object sender, EventArgs e)
        {
            Ticket filter = new Ticket(-1, "%", -1, -1, -1, -1, "%", DateTime.Now, "%");

            IList<object> lstResults = ticketbo.SelectManyObjects(filter);

            if (lstResults.Count <= 0)
            {
                lblResults.Text = "No Results Found";
                lblResults.ForeColor = System.Drawing.Color.Red;
                grdResults.DataSource = null;
                grdResults.DataBind();
            }
            else
            {
                lblResults.Text = string.Empty;
                grdResults.DataSource = lstResults;
                grdResults.DataBind();
                pnlResults.Visible = true;
            }

            lblMessage.Text = "Searched using 'Display All' Filter";
            lblMessage.ForeColor = System.Drawing.Color.Green;
        }

        protected void btnClearResults_Click(object sender, EventArgs e)
        {
            lblMessage.Text = string.Empty;
            lblResults.Text = "Results Cleared";
            lblResults.ForeColor = System.Drawing.Color.Green;
            grdResults.DataSource = null;
            grdResults.DataBind();
            pnlResults.Visible = false;
        }

        protected void grdResults_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                logger.Debug($"Edit Index: {e.NewEditIndex}");
                grdResults.EditIndex = e.NewEditIndex;
                PopulateResultsGrid();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        protected void grdResults_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GridViewRow row = grdResults.Rows[e.RowIndex];
                Label lblId = (Label)row.FindControl("Id");

                int ticketid = Convert.ToInt32(lblId.Text);
                logger.Debug($"Ticket ID: {ticketid}");

                temp = new Ticket(ticketid, "%", -1, -1, -1, -1, "%", DateTime.Now, "%");

                ticketbo.DeleteOneObject(temp);
                PopulateResultsGrid();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        protected void grdResults_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                logger.Debug($"Update Index: {e.RowIndex}");
                GridViewRow row = grdResults.Rows[e.RowIndex];

                Label lblId = (Label)row.FindControl("Id");
                Label lblDescription = (Label)row.FindControl("Description");
                Label lblBugId = (Label)row.FindControl("BugId");
                Label lblAppId = (Label)row.FindControl("SoftwareAppId");
                
                DropDownList drpPriorities = (row.FindControl("drpPriorities") as DropDownList);
                string priorityName = Convert.ToString(drpPriorities.SelectedItem); //name
                int priorityId = Convert.ToInt32(drpPriorities.SelectedValue); //id
                DropDownList drpStatuses = (row.FindControl("drpStatuses") as DropDownList);
                string statusName = Convert.ToString(drpStatuses.SelectedItem); //name
                int statusId = Convert.ToInt32(drpStatuses.SelectedValue); //id

                Label lblResolution = (Label)row.Cells[7].FindControl("Resolution");
                Label lblDateResolved = (Label)row.Cells[8].FindControl("DateResolved");

                TextBox txtResolution = (TextBox)row.Cells[9].FindControl("txtResolution");
                TextBox txtDateResolved = (TextBox)row.Cells[11].FindControl("txtDateResolved");


                int ticketId = Convert.ToInt32(lblId.Text);
                string description = Convert.ToString(lblDescription.Text);
                int bugId = Convert.ToInt32(lblBugId.Text);
                int appId = Convert.ToInt32(lblAppId.Text);
                string resolution = txtResolution.Text;
                string dateResolved = txtDateResolved.Text;



                //log statements -- see what's about to get updated with
                logger.Debug($"Ticket Id: {ticketId}");
                logger.Debug($"Description: {description}");
                logger.Debug($"Bug Id: {bugId}");
                logger.Debug($"SoftwareApp Id: {bugId}");
                logger.Debug($"Selected Priority from Dropdown [Update Mode]: {priorityId} {priorityName}");
                logger.Debug($"Selected Status from Dropdown [Update Mode]: {statusId} {statusName}");
                logger.Debug($"Resolution: {resolution}");
                logger.Debug($"Date Resolved : {dateResolved}"); 

                temp = new Ticket(ticketId, description, bugId, appId, priorityId, statusId, resolution, DateTime.Now, dateResolved);

                ticketbo.UpdateOneObject(temp);

                grdResults.EditIndex = -1;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            PopulateResultsGrid();

        }

        protected void grdResults_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            logger.Debug($"Cancel Index: {e.RowIndex}");
            grdResults.EditIndex = -1;
            PopulateResultsGrid();
        }

        protected void grdResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    DropDownList drpStatuses = (e.Row.FindControl("drpStatuses") as DropDownList);
                    drpStatuses.DataSource = statusbo.SelectManyObjects(new Status(-1, "%", DateTime.Now));
                    drpStatuses.DataValueField = "Id";
                    drpStatuses.DataTextField = "Name";
                    drpStatuses.DataBind();

                    DropDownList drpPriorities = (e.Row.FindControl("drpPriorities") as DropDownList);
                    drpPriorities.DataSource = prioritybo.SelectManyObjects(new Priority(-1, "%", DateTime.Now));
                    drpPriorities.DataValueField = "Id";
                    drpPriorities.DataTextField = "Name";
                    drpPriorities.DataBind();

                    
                }
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                logger.Error(ex);
            }
        }

   
    } //end of class; page
} //end of namespace